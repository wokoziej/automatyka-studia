# What is this repository for? #

* Items connected to my automation studiues

# Setup AUTODKURZ #

## Arduino ##
* Install Autodkurz.ino
* Debugging - Connect 128x32 IIC OLED 

## Raspberry PI3 ##
* Configure system 
* Pull and compile ros_packages
* Connect Arduino to RP3 throught the USB port
* Configure bluetooth

## Run tests on gazebo (step by step) ##

* Run gazebo node 

`roslaunch autodkurz_gazebo autodkurz.launch`
	
* Run teleop
    * if you using filtering 
	( `rosrun autodkurz_control autodkurz_command_filter.py` )
use mapping `/manual/cmd_vel` e.g.
	:::bash
	rosrun autodkurz_control autodkurz_key.py /cmd_vel:=/manual/cmd_vel`
	* if you want to connect teleop directly to driver_node
	`rosrun autodkurz_control autodkurz_key.py /cmd_vel:=/autodkurz_controller/cmd_vel'
* You can also run rviz 	
```
#!bash
roslaunch autodkurz_navigation rviz.launch 
```

## Run tests on real machine (step by step) ##
* Run autodkurz\_driver\_node	(remember to install packages like ros-kinetic-diff-drive-controller )

``` 
#!bash
roslaunch autodkurz_driver autodkurz_drive.launch 
```
* Run arduino communication node
```
#!bash
rosrun rosserial_python serial_node.py /dev/ttyACM0
```
* Run teleop
    * Using keys
```
	#!bash
	rosrun autodkurz_control autodkurz_key.py /cmd_vel:=/autodkurz_robota/cmd_vel
```
	* Using bloetooth 
	    * Change variable value BLUETOOTH_ENABLED to 1 in /etc/init.d/bluetooth
	    * Configure /etc/bluetooth/rfcomm.conf
```
rfcomm0 {
    # Automatically bind the device at startup
    bind yes;

    # Bluetooth address of the device
    device 98:D3:32:11:13:B1;

    # RFCOMM channel for the connection
    channel 1;

    # Description of the connection
    comment "RP3 Bluetooth Connection";
}
```
        * Bind device and run node which translates bluetooth messages into Twis message. Bluetooth message is in format (see https://bitbucket.org/wokoziej/automatyka-studia/src/80c91b78d4f7/arduino_skeches/joystick/?at=master) <number1, number2, number3> where 
		    * number1 is forward/backward speed
			* number2 is left/right turn
			* number3 button state
```
	#!bash
sudo rfcomm release rfcomm0 98:D3:32:11:13:B1
	sudo rfcomm bind rfcomm0 98:D3:32:11:13:B1	# You can add this to /etc/rc.local file
	rosrun autodkurz_control autodkurz_bluetooth.py /cmd_vel:=/autodkurz_robota/cmd_vel
```

## Run tests (one script) ##

