#include <L298N.h>


//pin definition

#define ENL 3 //3
#define IN1 4
#define IN2 5

#define IN3 8//4
#define IN4 9 //5

#define ENP 10 //3

int encoderAR = 19; //20;
int encoderBR = 18; //21;
int encoderAL = 20; //20;
int encoderBL = 21; //21;

//create a motor instance

int pulsesR;
int pulsesChangedR = 0;
int pulsesL;
int pulsesChangedL = 0;

L298N motorP(ENP, IN3, IN4);
L298N motorL(ENL, IN1, IN2);

void setup() {

  //used for display information
  Serial.begin(9600);

  motorL.setSpeed(40); // an integer between 0 and 255
  motorP.setSpeed(40); // an integer between 0 and 255

  //   pinMode(En_Motor_1, OUTPUT);
  //  digitalWrite(En_Motor_1, HIGH);

  pinMode(encoderAR, INPUT);
  pinMode(encoderBR, INPUT);
  attachInterrupt(digitalPinToInterrupt(encoderAR), A_CHANGE_R, CHANGE);

  
  pinMode(encoderAL, INPUT);
  pinMode(encoderBL, INPUT);
  attachInterrupt(digitalPinToInterrupt(encoderAL), A_CHANGE_L, CHANGE);
}

void loop() {
//  return;
  
  //tell the motor to go forward (may depend by your wiring)
  motorL.forward();
  motorP.forward();

  //print the motor satus in the serial monitor
  Serial.print("Is moving = ");
  Serial.println(motorL.isMoving());

  delay(1000);

  //stop running
  motorL.stop();
  motorP.stop();
/*
  Serial.print("Is moving = ");
  Serial.println(motorP.isMoving());

  delay(300);

  //change the initial speed
  motorL.setSpeed(10);

  //tell the motor to go back (may depend by your wiring)
  motorL.backward();
  motorP.backward();

  Serial.print("Is moving = ");
  Serial.println(motorL.isMoving());

  delay(300);

  //stop running
  motorL.stop();
motorP.stop();

  Serial.print("Is moving = ");
  Serial.println(motorL.isMoving());

  //change the initial speed
  motorL.setSpeed(20);

  Serial.print("Get new speed = ");
  Serial.println(motorL.getSpeed());

  delay(300);
*/
  if (pulsesChangedR != 0)
  {
    pulsesChangedR = 0;

    Serial.println(pulsesR);
  }

  
  if (pulsesChangedL != 0)
  {
    pulsesChangedL = 0;

    Serial.println(pulsesL);
  }
}

void change(int &encoderA, int &encoderB, int &pulses, int &pulseChange) 
{
  //Interrupt function to read the x2 pulses of the encoder.
  if ( digitalRead(encoderB) == 0 ) {
    if ( digitalRead(encoderA) == 0 ) {
      // A fell, B is low
      pulses--; // Moving forward
    } else {
      // A rose, B is high
      pulses++; // Moving reverse
    }
  } else {
    if ( digitalRead(encoderA) == 0 ) {
      pulses++; // Moving reverse
    } else {
      // A rose, B is low
      pulses--; // Moving forward
    }
  }
  pulseChange = 1;

}

void A_CHANGE_R() {
  change(encoderAR, encoderBR, pulsesR, pulsesChangedR);
  }


void A_CHANGE_L() {
  change(encoderAL, encoderBL, pulsesL, pulsesChangedL);
}
