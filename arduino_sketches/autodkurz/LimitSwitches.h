
enum LimitSwitches {
  RIGHT_FRONT_LIMIT_SWITCH,
  RIGHT_SIDE_LIMIT_SWITCH,
  RIGHT_BACK_LIMIT_SWITCH,
  LEFT_FRONT_LIMIT_SWITCH,
  LEFT_SIDE_LIMIT_SWITCH,
  LEFT_BACK_LIMIT_SWITCH,
  NO_OF_LIMIT_SWITCHES
};

struct Switch {
  int pin;
  int state;
  Switch (int _pin, int _state = HIGH)
    : pin  (_pin), state (_state) { }
};

#define LEFT_FRONT_LIMIT_SWITCH_PIN  36 
#define LEFT_SIDE_LIMIT_SWITCH_PIN   38 
#define LEFT_BACK_LIMIT_SWITCH_PIN   40 

#define RIGHT_FRONT_LIMIT_SWITCH_PIN 37
#define RIGHT_SIDE_LIMIT_SWITCH_PIN  39
#define RIGHT_BACK_LIMIT_SWITCH_PIN  41


Switch limitSwitches [LimitSwitches::NO_OF_LIMIT_SWITCHES] = {Switch (RIGHT_FRONT_LIMIT_SWITCH_PIN),
                                                              Switch(RIGHT_SIDE_LIMIT_SWITCH_PIN),
                                                              Switch(RIGHT_BACK_LIMIT_SWITCH_PIN),
                                                              Switch(LEFT_FRONT_LIMIT_SWITCH_PIN),
                                                              Switch(LEFT_SIDE_LIMIT_SWITCH_PIN),
                                                              Switch(LEFT_BACK_LIMIT_SWITCH_PIN)
                                                             };


void setupLimitSwitches() {
  for (int i = 0; i < LimitSwitches::NO_OF_LIMIT_SWITCHES; i++)
    pinMode (limitSwitches[i].pin, INPUT_PULLUP);
}

void barrierChecking () {
  barrierDetected = false;
  for (int i = 0; i < LimitSwitches::NO_OF_LIMIT_SWITCHES ; i++ ) {
    int state = digitalRead (limitSwitches [i].pin);
    limitSwitches [i].state = state;
    barrierDetected = barrierDetected || state == LOW;

    //    if (state != limitSwitches[i].state) {
    //      limitSwitches[i].state = state;
    //      // limitSwitchState.switch_id = limitSwitches[i].pin;
    //      // limitSwitchState.open = state == HIGH;
    //      //limitSwitchPublisher.publish(&limitSwitchState);
    //    }

#ifdef SWITCHES_DEBUG
    Serial.print (String (" ") + state + " ");
#endif

  }

#ifdef SWITCHES_DEBUG
  Serial.println();
#endif

}
