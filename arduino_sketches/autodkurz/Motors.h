#include <ros.h>
#include <std_msgs/Int8.h>
#include <L298N.h>

enum Motors {
  RIGHT_MOTOR,
  LEFT_MOTOR,
  NO_OF_MOTORS
};


#define ENA 3// 7
#define IN1 4 // 6
#define IN2 5 // 5 

int encoderAL = 20; //20;
int encoderBL = 21; //21;

#define IN3 8 // 3
#define IN4 9 // 8
#define ENB 10 // 4

int encoderAR = 19; //20;
int encoderBR = 18; //21;



#define MOTORS_ENABLED

extern bool barrierDetected;

class Encoder {

    int pulses;
    int pulseChange;
    int encoderA;
    int encoderB;

  public:
    Encoder (int APin, int BPin) {
      encoderA = APin;
      encoderB = BPin;
      pinMode(encoderA, INPUT);
      pinMode(encoderB, INPUT);
      pulses = 0;
      pulseChange = 0;
    }

    int getEncoderA () {
      return encoderA;
    }

    int getTicks() {
      return pulses;
    }

    void change()
    {
      //Interrupt function to read the x2 pulses of the encoder.
      if ( digitalRead(encoderB) == 0 ) {
        if ( digitalRead(encoderA) == 0 ) {
          // A fell, B is low
          pulses--; // Moving forward
        } else {
          // A rose, B is high
          pulses++; // Moving reverse
        }
      } else {
        if ( digitalRead(encoderA) == 0 ) {
          pulses++; // Moving reverse
        } else {
          // A rose, B is low
          pulses--; // Moving forward
        }
      }
      pulseChange = 1;
    }

};

class EncodedMotor {
    L298N *motor;
    Encoder *encoder;

  public:
    EncodedMotor (int EnablePin, int IN1Pin, int IN2Pin, int APin, int BPin) {
      motor = new L298N(EnablePin, IN1Pin, IN2Pin);
      encoder = new Encoder(APin, BPin);
    }

    Encoder *getEncoder () {
      return encoder;
    }

    L298N *getMotor () {
      return motor;
    }
};


EncodedMotor rightMotor (ENB, IN3, IN4, encoderAR, encoderBR);
EncodedMotor leftMotor (ENA, IN1, IN2, encoderAL, encoderBL);
EncodedMotor *motors [Motors::NO_OF_MOTORS] = {&rightMotor, &leftMotor};

void onChangleLeftEncoder() {
  leftMotor.getEncoder()->change();
}

void onChangleRightEncoder() {
  rightMotor.getEncoder()->change();
}

void setupMotors() {
  attachInterrupt(digitalPinToInterrupt( leftMotor.getEncoder()->getEncoderA() ), onChangleLeftEncoder, CHANGE);
  attachInterrupt(digitalPinToInterrupt( rightMotor.getEncoder()->getEncoderA() ), onChangleRightEncoder, CHANGE);
}

