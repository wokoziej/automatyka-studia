
class SensorsToSerial {
    HardwareSerial& serial;
    int i = 0;
    long time = 0;

  public:
    SensorsToSerial (HardwareSerial &_serial
                    ) : serial(_serial) {
    }

    void writeMsg () {
      String message;
      String leftMotorTicks (motors[LEFT_MOTOR]->getEncoder()->getTicks());
      String rightMotorTicks (motors[RIGHT_MOTOR]->getEncoder()->getTicks());
      String left_distance(leftDistanceSensor.minimalDistance());
      String right_distance(rightDistanceSensor.minimalDistance());
      
      String switches;

      for (int i = 0; i < LimitSwitches::NO_OF_LIMIT_SWITCHES ; i++ ) {
        switches += limitSwitches[i].state;
      }
      message = "T0S" + switches + "L" + leftMotorTicks + "R" + rightMotorTicks + "l" + left_distance + "r" + right_distance;

#ifdef MESSAGE_TO_SERIAL
      serial.println(message);
#endif

#ifdef DISTANCE_TO_SERIAL
      serial.println(distance);
#endif

    }

    void writeMessagePeriodically(int period) {
      if (millis() - time > period ) {
        writeMsg ();
        time = millis();
      }
    }
};

class SerialToDevices {
    HardwareSerial& serial;
    String message;

  public:
    SerialToDevices (HardwareSerial &_serial) : serial(_serial) {
    }

    void readAndApplyMessage() {
      if ( serial.available()) {
        char c = serial.read();
        if (c != '\n')
          message = message + c ;

        if (c == '\n') {
          char messageType = message.charAt(0);
          switch (messageType) {
            case 'R':
            case 'L': {
                char wheelSide = messageType;
                char wheelDir = message.charAt(1);
                int wheelSpeed = message.substring(2).toInt();
                wheelSpeedChange(wheelSide, wheelDir, wheelSpeed) ;
                break;
              }
            case 'V': {
                char vacuumOn = message.charAt(1);
                switchVacuum (vacuumOn == '1');
                break;
              }
            case 'D': {
                char distanceSensorSwitch = message.charAt(1);
                switchTurningServos (distanceSensorSwitch == '1');
                break;
              }
          }
          message = "";

        }
      }

    }
  protected:
    void wheelSpeedChange(char motorSide, char direction, byte wheelSpeed) {
      int speed = min(wheelSpeed, 255);
      int motorIndex = motorSide == 'R' ? 0 : 1;
#ifdef MOTORS_ENABLED
      L298N *motor = motors [motorIndex]->getMotor();
      motor->setSpeed(speed);
      if (direction == 'S' //|| barrierDetected
         )
        motor->stop();
      else if (direction == 'F' ) {
        motor->forward();
      }
      else if (direction == 'B' ) {
        motor->backward();
      }
#endif
    }

    void switchVacuum (bool On) {
      if (On)
        vacuumCleanerOn();
      else
        vacuumCleanerOff();
    }

    void switchTurningServos (bool On) {
      if (On)
        servosOn ();
      else
        servosOff();
    }
};



