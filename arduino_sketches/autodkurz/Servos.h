#include <Servo.h>

#define LEFT_SERVO_PIN 49
#define RIGHT_SERVO_PIN 48

int angleInUs = 650;
int cycleTimeInMs = 2000;
int frequencyInMs = 120;
// detachPeriod have to be less then frequency !!!
int detachPeriodInMs = 50;


enum Servos {
  RIGHT_SERVO,
  LEFT_SERVO,
  NO_OF_SERVOS
};


class CyclicServo {
    Servo sg90;
    int cycleTime;
    int angleInUs;
    unsigned long time;
    int  middleMicroseconds;
    int minMicroseconds;
    int maxMicroseconds;
    unsigned long frequency;
    unsigned long lastUpdate;
    int pin;
    int detachPeriodInMs;
  public:
    CyclicServo (int _pin, int _angleInUs, int _period, int _frequency, int _detachPeriodInMs) : pin(_pin), angleInUs(_angleInUs), cycleTime(_period), frequency(_frequency), detachPeriodInMs(_detachPeriodInMs) {

      middleMicroseconds = 1500;
      maxMicroseconds = middleMicroseconds + angleInUs / 2 ;
      minMicroseconds = middleMicroseconds - angleInUs / 2 ;
    }

    void initialize() {
      sg90.attach (pin);
      time = millis();
      setInTheMiddle();
      sg90.detach();
    }

    void cycle() {
      unsigned long m = millis();

      if (m - lastUpdate > frequency) {
        time = m % cycleTime;
        int currentAngle = calculateCurretAngle();

#ifdef AUTODKURZ_DEBUG
        Serial.println(" Angle:" + String(currentAngle));
#endif

        sg90.attach (pin);
        sg90.writeMicroseconds(currentAngle);

        lastUpdate = m;
      }

      if (m - lastUpdate > detachPeriodInMs)
        sg90.detach();
    }

    void stop () {
      sg90.detach();
    }

    void restart () {
      sg90.attach (pin);
      time = millis();
    }

  protected:
    int calculateCurretAngle() {
      float timeInPCT = time * 1.0 / cycleTime;

#ifdef AUTODKURZ_DEBUG
      Serial.println("Time %:" + String(timeInPCT));
#endif
      int angle;
      if (timeInPCT <= 0.25) {
        int a = maxMicroseconds - middleMicroseconds;
        angle = middleMicroseconds + a * (timeInPCT / 0.25);
      } else if (timeInPCT <= 0.75) {
        int a = maxMicroseconds - minMicroseconds;
        angle = maxMicroseconds - a * ( (timeInPCT - 0.25) / 0.50);
      } else {
        int a = middleMicroseconds - minMicroseconds;
        angle = minMicroseconds + a * ( (timeInPCT - 0.75) / 0.25);

      }

      return angle;
    }

    void setInTheMiddle() {
      sg90.writeMicroseconds(middleMicroseconds);
    }
};


CyclicServo leftServo(LEFT_SERVO_PIN, angleInUs + 50, cycleTimeInMs , frequencyInMs, detachPeriodInMs);
CyclicServo rightServo(RIGHT_SERVO_PIN, angleInUs , cycleTimeInMs, frequencyInMs, detachPeriodInMs);







class ServosCycler {
    bool cycleOn;
  public:
    ServosCycler() {
      cycleOn = false;
    }

    void initialize() {
      leftServo.initialize();
      rightServo.initialize();

    }

    void cycle () {
      if (cycleOn) {
        leftServo.cycle();
        rightServo.cycle();
      }
    }

    void turnOn() {
      cycleOn = true;
      leftServo.restart();
      rightServo.restart();
    }
    void turnOff() {
      cycleOn = false;
      leftServo.stop();
      rightServo.stop();
    }


};


ServosCycler servosCycler;
void setupServos() {
  servosCycler.initialize();
}

void servosOn() {
  servosCycler.turnOn();
}

void servosOff() {
  servosCycler.turnOff();
}
