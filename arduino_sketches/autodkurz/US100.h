#define GET_DISTANCE  0x55
#include <SoftwareSerial.h>
#include <limits.h>

#define NO_OF_DISTANCES_TO_REMEMBER 60

#define PAUSE_BETWEEN_READS 100
int rxRight = 52;
int txRight = 53;
int rxLeft = 50;
int txLeft = 51;

int SENSOR_READ_TIMEOUT = 400;

enum DistanceSensors {
  RIGHT_SENSOR,
  LEFT_SENSOR,
  NO_OF_SENSORS
};


enum ReadStatus {
  ASK_FOR_DISTANCE,
  HIGH_BYTE,
  LOW_BYTE,
  COMPLETE
};

class US100 {
    int txPin;
    int rxPin;
    int mmDist;
    ReadStatus readStatus;
    unsigned long pauseBetweenReads;
    unsigned long readTimeout;
    unsigned long lastReadTime;
    unsigned long lastAskForDistanceTime;
    uint8_t highDist;
    uint8_t lowDist;
    SoftwareSerial *us100serial;
    String us100name;

    int distancesArray [NO_OF_DISTANCES_TO_REMEMBER];
    int minDistanceFromArray;

  public:
    US100(int _txPin, int _rxPin, int _readTimeout, unsigned long _pauseBetweenReads = PAUSE_BETWEEN_READS) :
      txPin(_txPin),
      rxPin (_rxPin),
      readTimeout(_readTimeout),
      pauseBetweenReads(_pauseBetweenReads)
    {
      us100serial = new SoftwareSerial(rxPin, txPin);
      readStatus = ASK_FOR_DISTANCE;
      lastReadTime = 0;
      lastAskForDistanceTime = 0;
      readTimeout = 2000;
      us100name = String ("TX_") + txPin + "_RX_" + rxPin  ;
      for (int i = 0; i < NO_OF_DISTANCES_TO_REMEMBER; i++)
        distancesArray[i] = 0;
      minDistanceFromArray = 0;

    }

    void initialize () {
      pinMode(rxPin, INPUT);
      pinMode(txPin, OUTPUT);
      us100serial->begin(9600);
    }

    bool isCheckingDistance() {
      return readStatus != ReadStatus::COMPLETE;
    }

    bool listen () {
      return us100serial->listen();

    }
    void checkDistance () {
      //      if ( isCheckingDistance() )

      switch (readStatus) {
        case  ASK_FOR_DISTANCE:
          us100serial->flush();
          us100serial->write(GET_DISTANCE);
          readStatus = (readStatus + 1);
          lastAskForDistanceTime = millis();

#ifdef AUTODKURZ_DEBUG
          Serial.println(us100name + ": Asking about distance...");
#endif
          break;
        case HIGH_BYTE:
          byteRead (highDist);
          break;
        case LOW_BYTE:
          if ( byteRead (lowDist) ) {
            mmDist = highDist * 256 + lowDist;
            lastReadTime = millis();
          }
          break;
        case COMPLETE:
#ifdef AUTODKURZ_DEBUG
          Serial.println(String(us100name + ":Have recorded distance = ") + String(getDistance()));
#endif
          if (millis () - lastReadTime > pauseBetweenReads) {
            readStatus = ASK_FOR_DISTANCE;
          }
          break;
      }

    }

    int getDistance() {
      return mmDist;
    }

    int minimalDistance() {
      int distance = getDistance();
      //int rightDistance = distance_sensors[RIGHT_SENSOR]->getDistance();
      int currentMinimal = getDistance(); //min (leftDistance, rightDistance);

      memmove(distancesArray + 1, distancesArray, (NO_OF_DISTANCES_TO_REMEMBER - 1) * sizeof(*distancesArray));

#ifdef AUTODKURZ_DEBUG
      Serial.print("Shifted array");
      for (int i = 0; i < NO_OF_DISTANCES_TO_REMEMBER; i++)
        Serial.print( " " + String(distancesArray[i]) );
      Serial.println("");
#endif

      distancesArray[0] = currentMinimal;

      if (currentMinimal < minDistanceFromArray)
        minDistanceFromArray = currentMinimal;
      else {
        // We have to calculate min distance again, but can omit first element
        minDistanceFromArray = INT_MAX;
        for (int i = 1; i < NO_OF_DISTANCES_TO_REMEMBER; i++)
          if (minDistanceFromArray > distancesArray[i])
            minDistanceFromArray = distancesArray[i];
      }
      return minDistanceFromArray;
    }

  private:
    bool byteRead (uint8_t &oneByte) {
      if (us100serial->available()) {
        oneByte = us100serial->read();
        readStatus = (readStatus + 1);
      } else

        if (millis () - lastAskForDistanceTime > readTimeout ) {
          // reset reading
          readStatus = ASK_FOR_DISTANCE;
#ifdef AUTODKURZ_DEBUG
          Serial.println(us100name + ":Timeout reading distance...");
#endif
        }

    }

};


US100 leftDistanceSensor (txLeft, rxLeft, SENSOR_READ_TIMEOUT);
US100 rightDistanceSensor (txRight, rxRight, SENSOR_READ_TIMEOUT);
US100 *distance_sensors [DistanceSensors::NO_OF_SENSORS] = {&rightDistanceSensor, &leftDistanceSensor};

void setupDistanceSensors() {
  leftDistanceSensor.initialize();
  rightDistanceSensor.initialize();
}

class DistanceChecker {
    int currentSensorIndex = 0;
    long unsigned int lastCheckTime;
   
  public:

    DistanceChecker () {
    }

    void setup () {
      distance_sensors [currentSensorIndex]->listen();
    }

    void check () {

      US100 *currentSensor = distance_sensors [currentSensorIndex];

#ifdef AUTODKURZ_DEBUG
      Serial.println(String(currentSensorIndex) + " : checking distance..., last check time" + String(lastCheckTime) );
#endif

      if (millis () - lastCheckTime < SENSOR_READ_TIMEOUT ) {
        currentSensor->checkDistance();
        if ( ! currentSensor->isCheckingDistance() )
          currentSensorIndex = (currentSensorIndex + 1) % DistanceSensors::NO_OF_SENSORS;
        distance_sensors [currentSensorIndex]->listen();
        lastCheckTime = millis();
      }

    }

};

DistanceChecker distanceChecker;






