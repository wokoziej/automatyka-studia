
  
L298N brushRight(0, 32, 33);
L298N brushLeft(0, 30, 31);


void vacuumCleanerOn () {
  brushRight.backward();
  brushLeft.forward();  
}


void vacuumCleanerOff () {
  brushRight.stop();
  brushLeft.stop();  
}

void setupVacuumCleaner() {
}
