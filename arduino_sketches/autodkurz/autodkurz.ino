// Messages on
#define MESSAGE_TO_SERIAL

//#define AUTODKURZ_DEBUG
//#define SWITCHES_DEBUG

//#define DISTANCE_TO_SERIAL

#include "Motors.h"
#include "MPU.h"
#include "US100.h"
#include "Servos.h"
#include "VacuumCleaner.h"
#include "LimitSwitches.h"
#include "SerialCommunication.h"

bool barrierDetected = true;



void setup() {
  Serial.begin (115200);
  setupLimitSwitches();
  //setupMPU();
  setupDistanceSensors();
  setupMotors();
  setupServos();
  setupVacuumCleaner();
}






SerialToDevices serial2speed (Serial);
SensorsToSerial sensors2serial(Serial);


void loop() {

  barrierChecking();
  //delay (100);
  //if (barrierDetected) {
  //  stopMotors ();
  //}
  
  servosCycler.cycle();
  distanceChecker.check();
  serial2speed.readAndApplyMessage() ;
  sensors2serial.writeMessagePeriodically(20);
}

