#include <AutoPID.h>


int sensor1Pin = A1;    // select the input pin for the potentiometer
int sensor2Pin = A2;    // select the input pin for the potentiometer
int buttonPin = 13;    // select the input pin for the potentiometer

// select the pin for the LED
int sensor1Value = 0;  // variable to store the value coming from the sensor
int sensor2Value = 0;  // variable to store the value coming from the sensor
int buttonValue = HIGH;

int sensor1ValueMem = 1;  // variable to store the value coming from the sensor
int sensor2ValueMem = 1;  // variable to store the value coming from the sensor
int buttonValueMem = HIGH;

int btRX = 3;
int btTX = 2;


#include <SoftwareSerial.h>

SoftwareSerial bluetooth (btRX, btTX);
//SoftwareSerial bluetooth(0, 1);


void setup() {
  Serial.begin(9600);
  bluetooth.begin(9600);
  pinMode(buttonPin, INPUT_PULLUP);
}


int ommitNearZero(int i, int e) {
  
  if (abs(i) < e) return 0;
  return i;
}

void updateMinMax (int &min, int &max, int v)
{
  if (min > v)
  min = v;
  if (max < v)
  max = v;
}
int max1 = 0, min1=1024, max2 = 0, min2 = 1024;
void loop() {
  // read the value from the sensor:
  int sensMem = sensor1Value;
  
  sensor1Value = ommitNearZero (analogRead(sensor1Pin) - 510, 10) ;
  sensor2Value = ommitNearZero (analogRead(sensor2Pin) - 526, 10);

  updateMinMax(min1, max1, sensor1Value);
  updateMinMax(min2, max2, sensor2Value);
  
  buttonValue = digitalRead(buttonPin) ;

  if (abs (sensor1Value - sensor1ValueMem) > 2 ||
      abs (sensor2Value - sensor2ValueMem) > 2 || 
      buttonValue != buttonValueMem)
  {
    String a = String(sensor1Value) + ";" + sensor2Value + ";" + buttonValue + ";";
    char chksum = 0;
    for (short c = a.length() -1; c >=0; c--) {
       chksum ^= a.charAt(c);
       // Serial.println(String () +  a.charAt(c) + " " + c +  " " + String((short) chksum) + " " + chksum);
    }
    if (chksum == '\n' || chksum == '\r')
       chksum = '!';
       
    a += chksum;    
    Serial.println(a);
    
    //bluetooth.print (a);
    
    for (int c = 0; c < a.length();  c++)
      bluetooth.print (a.charAt(c));
    
    bluetooth.flush();
    
    bluetooth.println();
    //delay(30);
    bluetooth.flush();
    //delay(30);
    
    sensor1ValueMem = sensor1Value;
    sensor2ValueMem = sensor2Value;
    buttonValueMem = buttonValue;
    delay(50);
  }
}
