#ifndef _ROS_autodkurz_control_LimitSwitchState_h
#define _ROS_autodkurz_control_LimitSwitchState_h

#include <stdint.h>
#include <string.h>
#include <stdlib.h>
#include "ros/msg.h"

namespace autodkurz_control
{

  class LimitSwitchState : public ros::Msg
  {
    public:
      typedef int32_t _switch_id_type;
      _switch_id_type switch_id;
      typedef bool _open_type;
      _open_type open;

    LimitSwitchState():
      switch_id(0),
      open(0)
    {
    }

    virtual int serialize(unsigned char *outbuffer) const
    {
      int offset = 0;
      union {
        int32_t real;
        uint32_t base;
      } u_switch_id;
      u_switch_id.real = this->switch_id;
      *(outbuffer + offset + 0) = (u_switch_id.base >> (8 * 0)) & 0xFF;
      *(outbuffer + offset + 1) = (u_switch_id.base >> (8 * 1)) & 0xFF;
      *(outbuffer + offset + 2) = (u_switch_id.base >> (8 * 2)) & 0xFF;
      *(outbuffer + offset + 3) = (u_switch_id.base >> (8 * 3)) & 0xFF;
      offset += sizeof(this->switch_id);
      union {
        bool real;
        uint8_t base;
      } u_open;
      u_open.real = this->open;
      *(outbuffer + offset + 0) = (u_open.base >> (8 * 0)) & 0xFF;
      offset += sizeof(this->open);
      return offset;
    }

    virtual int deserialize(unsigned char *inbuffer)
    {
      int offset = 0;
      union {
        int32_t real;
        uint32_t base;
      } u_switch_id;
      u_switch_id.base = 0;
      u_switch_id.base |= ((uint32_t) (*(inbuffer + offset + 0))) << (8 * 0);
      u_switch_id.base |= ((uint32_t) (*(inbuffer + offset + 1))) << (8 * 1);
      u_switch_id.base |= ((uint32_t) (*(inbuffer + offset + 2))) << (8 * 2);
      u_switch_id.base |= ((uint32_t) (*(inbuffer + offset + 3))) << (8 * 3);
      this->switch_id = u_switch_id.real;
      offset += sizeof(this->switch_id);
      union {
        bool real;
        uint8_t base;
      } u_open;
      u_open.base = 0;
      u_open.base |= ((uint8_t) (*(inbuffer + offset + 0))) << (8 * 0);
      this->open = u_open.real;
      offset += sizeof(this->open);
     return offset;
    }

    const char * getType(){ return "autodkurz_control/LimitSwitchState"; };
    const char * getMD5(){ return "0851912e3d3488a2fc02a6f0caa46cc7"; };

  };

}
#endif