
#include <ros.h>
#include <std_msgs/Bool.h>
#include "LimitSwitchState.h"

//#define LSC_DEBUG

ros::NodeHandle nh;

autodkurz_control::LimitSwitchState limitSwitchState;
ros::Publisher limitSwitchPublisher("switches", &limitSwitchState);

struct Switch {
  int pin;
  int state;
  Switch (int _pin, int _state = HIGH)
    : pin  (_pin), state (_state) { }
};


Switch  switches [] =  {Switch(3), Switch(5)};
int switchCount = sizeof(switches) / sizeof(*switches);

void setup() {
  nh.initNode();
  nh.advertise(limitSwitchPublisher);
  
  for (int i = 0; i < switchCount; i++)
    pinMode (switches[i].pin, INPUT_PULLUP);

#ifdef LSC_DEBUG
  Serial.begin(9600);
#endif

}

void loop() {
  
  for (int i = 0; i < switchCount; i++) {
    int state = digitalRead(switches[i].pin);
    if (state != switches[i].state) {
      switches[i].state = state;
      limitSwitchState.switch_id = switches[i].pin;
      limitSwitchState.open = state == HIGH;
  
      limitSwitchPublisher.publish(&limitSwitchState);
      
#ifdef LSC_DEBUG
      Serial.print ("switch = ");
      Serial.print(limitSwitchState.switch_id);
      Serial.print (", open = ");
      Serial.println (limitSwitchState.open);
#endif

    }
  }
  nh.spinOnce();
  delay(10);
}
