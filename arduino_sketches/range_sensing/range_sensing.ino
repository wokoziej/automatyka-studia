/*
   rosserial IR Ranger Example

   This example is calibrated for the Sharp GP2D120XJ00F.
*/

#include <ros.h>
#include <ros/time.h>
#include <sensor_msgs/Range.h>

#include <ST_HW_HC_SR04.h>

// Change the pins if you wish.
ST_HW_HC_SR04 ultrasonicSensor(13, 12); // ST_HW_HC_SR04(TRIG, ECHO)


ros::NodeHandle  nh;


sensor_msgs::Range range_msg;
ros::Publisher pub_range( "range_data", &range_msg);

unsigned long range_timer;

char frameid[] = "/ultra_ranger";

void setup()
{
  Serial.begin(9600);

  while (!Serial); // Wait for the Serial connection;

  nh.initNode();
  nh.advertise(pub_range);

  range_msg.radiation_type = sensor_msgs::Range::ULTRASOUND;
  range_msg.header.frame_id =  frameid;
  range_msg.field_of_view = 0.01;
  range_msg.min_range = 0.001;
  range_msg.max_range = 0.7;

}

void loop()
{

  int hitTime = ultrasonicSensor.getHitTime(); // In microseconds

  /*
    #
    # This means the sensor didn't receive back the packets before the timeout
    #
    #   That usually happens when the distance between the sensor and the block-
    # ing object is greater than 86 centimeters (33.85 inches).
    #
    #   Increasing the timeout from 5000 microseconds to 23200 microseconds
    # will increase the maximum distance to ~4m, at the cost of the code being
    # stuck for 23.2 ms if no packet is received during this period (worst case
    # scenario).
    #
    #   More information (including calculations) is included in the
    # extras/HC-SR04.txt file. (Inside your libraries/ST_HW_HC_SR04 folder)
    #
  */
  if ((hitTime == 0) && (ultrasonicSensor.getTimeout() == 5000)) {
    ultrasonicSensor.setTimeout(23200);

   
  } else {
    int distanceInCm = hitTime / 29;

    //Serial.print(distanceInCm);
    //Serial.println();

    if ( (millis() - range_timer) > 50) {
      range_msg.range = distanceInCm/ 100.0;

      range_msg.header.stamp = nh.now();
      pub_range.publish(&range_msg);
      range_timer =  millis() + 50;
    }
    nh.spinOnce();

  }


  delay(50);


}

