#include <Servo.h>

// Stworzenie obiektu serwomechanizmu
Servo sg90;

void setup()
{
  // Ustawienie pinu do ktorego podlaczone jest serwo
  sg90.attach(48);
}

void loop()
{


  for (int i = 700; i < 2450; i = i + 20) {

    // ustawienie osi na minimalny kat
  sg90.writeMicroseconds(i);
  
  // oczekiwanie pol sekundy
  delay(10);
  
  }
  
  for (int i = 2450; i > 700; i = i - 20) {

    // ustawienie osi na minimalny kat
  sg90.writeMicroseconds(i);
  
  // oczekiwanie pol sekundy
  delay(10);
  
  }
  
}
