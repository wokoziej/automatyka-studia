#!/usr/bin/python
# Copyright (c) 2014 Adafruit Industries
# Author: Tony DiCola
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

import RPi.GPIO as GPIO 
import time
from time import gmtime, strftime
import Adafruit_Nokia_LCD as LCD
import Adafruit_GPIO.SPI as SPI

from PIL import Image
from PIL import ImageDraw
from PIL import ImageFont

import datetime


# Raspberry Pi hardware SPI config:
DC = 23
RST = 24
SPI_PORT = 0
SPI_DEVICE = 0

# Raspberry Pi software SPI config:
# SCLK = 4
# DIN = 17
# DC = 23
# RST = 24
# CS = 8

# Beaglebone Black hardware SPI config:
# DC = 'P9_15'
# RST = 'P9_12'
# SPI_PORT = 1
# SPI_DEVICE = 0

# Beaglebone Black software SPI config:
# DC = 'P9_15'
# RST = 'P9_12'
# SCLK = 'P8_7'
# DIN = 'P8_9'
# CS = 'P8_11'


# Hardware SPI usage:
disp = LCD.PCD8544(DC, RST, spi=SPI.SpiDev(SPI_PORT, SPI_DEVICE, max_speed_hz=4000000))

# Software SPI usage (defaults to bit-bang SPI interface):
#disp = LCD.PCD8544(DC, RST, SCLK, DIN, CS)

# Initialize library.
disp.begin(contrast=60)

# Clear display.
disp.clear()
disp.display()

# Create blank image for drawing.
# Make sure to create image with mode '1' for 1-bit color.
image = Image.new('1', (LCD.LCDWIDTH, LCD.LCDHEIGHT))

# Get drawing object to draw on image.
draw = ImageDraw.Draw(image)

# Draw a white filled box to clear the image.
draw.rectangle((0,0,LCD.LCDWIDTH,LCD.LCDHEIGHT), outline=255, fill=255)

# Draw some shapes.
#draw.ellipse((2,2,22,22), outline=0, fill=255)
#draw.rectangle((24,2,44,22), outline=0, fill=255)
#draw.polygon([(46,22), (56,2), (66,22)], outline=0, fill=255)
#draw.line((68,22,81,2), fill=0)
#draw.line((68,2,81,22), fill=0)

# Load default font.
font = ImageFont.load_default()

# Alternatively load a TTF font.
# Some nice fonts to try: http://www.dafont.com/bitmap.php
# font = ImageFont.truetype('Minecraftia.ttf', 8)

# Write some text.
#draw.text((8,30), 'Hello World!', font=font)

# Display image.
disp.image(image)
disp.display()
port_or_pin=26
GPIO.setup(port_or_pin, GPIO.OUT)
GPIO.output(port_or_pin, 0) 

print('Press Ctrl-C to quit.')

#draw.setRotation(180);
import subprocess

cpuCount = int(subprocess.Popen("grep 'model name' /proc/cpuinfo | wc -l",
                                shell=True, stdout=subprocess.PIPE).stdout.read())


# Some nice fonts to try: http://www.dafont.com/bitmap.php
font = ImageFont.truetype('04B_30__.TTF', 8)
draw.text((8,0), 'Autodkurz!', font=font)

draw.line((10,10,LCD.LCDWIDTH-10,10), fill=0)

fontDef = ImageFont.load_default()
start = 12
draw.text((0, start), "{}".format("load"), font=fontDef)
draw.text((0, 2 * start), "{}".format(" cpu"), font=fontDef)

while True:
    time.sleep(3.0)


    cpu = float(
        subprocess.Popen("ps -A -o pcpu | tail -n+2 | paste -sd+ | bc",
                         shell=True, stdout=subprocess.PIPE).stdout.read()) / (cpuCount * 100)



    file = open("/proc/loadavg", "r") 
    loadAvg = file.read().split()
        
    oneMin = float(loadAvg[0]) / cpuCount
    fiveMin = float(loadAvg[1]) / cpuCount
    fifteenMin = float(loadAvg[2]) / cpuCount
    width = 3
    textStop = 28
    
    displayWidth = LCD.LCDWIDTH - textStop

    draw.rectangle([textStop, start, LCD.LCDWIDTH, LCD.LCDHEIGHT], fill=255)

    draw.rectangle([textStop, start + 0, textStop + displayWidth * oneMin , start + width-1], fill=0)
    draw.rectangle([textStop, start + width, textStop + displayWidth * fiveMin, start + 2*width-1], fill=0)
    draw.rectangle([textStop, start + 2*width, textStop + displayWidth * fifteenMin, start + 3*width-1], fill=0)


    draw.rectangle([textStop, 2*start, textStop + displayWidth * cpu, 2*start + 3*width-1], fill=0)

        
#    draw.text((0, 0), "{}".format(f[0])) 
#    draw.text((0, step), "up {}".format(f[2]))

    
    disp.image(image)
    disp.display()
