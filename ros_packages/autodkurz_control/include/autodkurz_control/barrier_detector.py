import rospy
from autodkurz_control.msg import LimitSwitchState

class BarrierDetector():

    def __init__(self):
        rospy.Subscriber('/switches', LimitSwitchState, self.onBarrierDetected)
        self.barrierId = -1
        self.barrierDetectionTime = rospy.Time(0)
        self.switchesMem = [-1,-1,-1,-1,-1,-1]

        
    def onBarrierDetected(self, msg):
        
        if msg.open != True or True: # Always for real mashine, gazebo has another mechanism:            
            #self.barrierDetected = not msg.open;
            self.barrierId = msg.switch_id
            self.switchesMem[self.barrierId] = int(not msg.open)
            self.barrierDetectionTime = rospy.Time.now();

    def inCollision (self):
        for i in self.switchesMem:
            if i > 0:
                return True

        return False
        #return (
            #  self.barrierId != -1 and rospy.Time.now() - self.barrierDetectionTime < rospy.Duration (0.25)
            
        #)
    
    def collisionSide (self):
        if self.barrierId > 2:
            return 'left'
        else:
            if self.barrierId <= 2:
                return 'right'
        return 'none'

    def sideBarrierIndex(self):
        if self.barrierId > 2:
            return self.barrierId - 3
        return self.barrierId

