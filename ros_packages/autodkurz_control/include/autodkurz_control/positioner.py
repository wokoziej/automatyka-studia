import rospy
import numpy
from tf import TransformListener
from tf.transformations import euler_from_quaternion

class Positioner:
    
    def __init__(self, base_frame_id = '/base_footprint', map_frame_id = '/odom'):
    	self.base_frame_id = base_frame_id
        self.map_frame_id = map_frame_id
        self.tf = TransformListener()        

    def getTransform(self):
        trials = 10
        rate = rospy.Rate(trials)
        i = 0
        while i < trials:
            try:
                i += 1
                t = self.tf.getLatestCommonTime(self.base_frame_id, self.map_frame_id)
                position, quaternion = self.tf.lookupTransform(self.map_frame_id, self.base_frame_id, t)
                return position, quaternion            
            except Exception as e:
                rospy.loginfo("Cannot lookupTransform: {}, sleeping {}, ".format(str(e), i))
                rospy.sleep(i)
                
        return None, None                
            
    def getPosition(self):
        position, quaternion = self.getTransform()
        return numpy.array((position[0], position[1]))

    def getYaw(self):
        position, quaternion = self.getTransform()
        (roll,pitch,yaw) = euler_from_quaternion(quaternion)
        return yaw
