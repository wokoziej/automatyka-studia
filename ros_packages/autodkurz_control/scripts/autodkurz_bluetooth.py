#!/usr/bin/env python

import subprocess
import rospy
import serial

from geometry_msgs.msg import Twist

import sys, select, termios, tty


def stopMessage(pub):
    twist = Twist()
    twist.linear.x = 0; twist.linear.y = 0; twist.linear.z = 0
    twist.angular.x = 0; twist.angular.y = 0; twist.angular.z = 0
    print "Stop message"
    pub.publish(twist)


if __name__=="__main__":
    
    rospy.init_node('bt_teleop', anonymous=True)
    pub = rospy.Publisher('/cmd_vel', Twist, queue_size=10)
    
    x = 0
    th = 0
    status = 0
    count = 0
    acc = 0.1
    target_speed = 0
    target_turn = 0
    control_speed = 0
    control_turn = 0
    ser = serial.Serial("/dev/rfcomm0")
    while 1==1:

        #proc = subprocess.Popen(['cat','/dev/rfcomm0'], stdout=subprocess.PIPE, shell=False)

        try:
            twist = Twist()
            twist.linear.y = 0;
            twist.linear.z = 0
            twist.angular.x = 0;
            twist.angular.y = 0;

#	    for line in iter(proc.stdout.readline,''):
 	    line = ser.readline()
	    if	len(line) > 0:
		chksum = 0
		line = line.replace('\n', '').replace('\r', '')
		if len(line) < 6:
	           rospy.loginfo("Line to short")
		   continue;
		for c in line[-2::-1]:
		    chksum = chksum ^ ord(c)
                    # print c, chksum 
                expected= line[-1]
		if chr(chksum) in ('\n', '\r'):
		   chksum = ord('!')
 
                if chr(chksum) != expected:


		    rospy.logerr("Invalid chksum for message '{}', should be '{}', is '{}'".format(line, expected, chr(chksum)))
		    for c in line[-2::-1]:
		       chksum = chksum ^ ord(c)
                       print c, ord(c), chksum, chr(chksum)

		    stopMessage(pub)

  		else:
	            #rospy.loginfo("LINE: {}".format(line))
	            splitedLine = line.rstrip().split(";")
                    if len(splitedLine) > 1:
                        try:
            	            control_speed = float( splitedLine[0] ) / 512.0 * 0.2
            	            control_turn = float( splitedLine[1] ) / 512.0 * 0.9
			    #print "speed", control_speed, "turn", control_turn
                            #if twist.linear.x != control_speed or twist.angular.z != control_turn:
            	            twist.linear.x = control_speed
                            twist.angular.z = control_turn
        	            pub.publish(twist)
                        except Exception as e:
                            print e
			    stopMessage(pub)
                            rospy.logerr("Convertion problem? {} ".format(str(e)))

        except Exception as e:
            print e
	    stopMessage(pub)

            rospy.logerr("Err, sleeping 5 sec:\n" + str(e))            
            exit(0)
         


