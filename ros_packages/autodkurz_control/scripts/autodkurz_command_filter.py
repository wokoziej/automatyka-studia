#!/usr/bin/env python


#
# Script can be used for /cmd_vel filtering.
# If we detect barrier (/switches) we send "zero" as twist message to _DESTINATION_TOPIC_NAME
# instead of real values from _MANUAL... OR _AUTO_TOPIC_NAME
#


import geometry_msgs;
from geometry_msgs.msg import Twist
import message_filters;
from message_filters import Subscriber
import roslib;
import rospy
import autodkurz_control
from autodkurz_control.msg import LimitSwitchState
import smach
import smach_ros

_RESEND = "RESEND"
_BRAKE = "BRAKE"

_MANUAL_TOPIC_NAME = "/manual/cmd_vel"
_AUTO_TOPIC_NAME = "/auto/cmd_vel"
_DESTINATION_TOPIC_NAME = '/autodkurz_controller/cmd_vel'
_BARRIER_TOPIC_NAME = "/switches"

class CollisionChecker:
    def __init__(self):
        self.switchesSub = rospy.Subscriber(_BARRIER_TOPIC_NAME,
                                            autodkurz_control.msg.LimitSwitchState,
                                            self.switchesChanged)
        self.switches = {};
        self.barrierDetected = False;
        
    def switchesChanged (self, switch):
        self.switches[switch.switch_id] = switch.open
        self.barrierDetected = False
        for value in self.switches.itervalues():
            self.barrierDetected = self.barrierDetected or not value

        # print 'switches', self.switches
        # print 'detected', self.barrierDetected
        
    def moveIsColliding(self):
        return self.barrierDetected
    
class BrakeFilter():
    def __init__(self, collisionChecker):
        self.cmdVelPub = rospy.Publisher(_DESTINATION_TOPIC_NAME, Twist, queue_size = 1)    
        sub = message_filters.Subscriber(_MANUAL_TOPIC_NAME , geometry_msgs.msg.Twist)
        sub.registerCallback(self.filterCallback)
        sub = message_filters.Subscriber(_AUTO_TOPIC_NAME, geometry_msgs.msg.Twist)
        sub.registerCallback(self.filterCallback)
        self.collisionChecker = collisionChecker;
        self.state = _RESEND
        self.zeroTwistMessage = Twist ()

    def cmdVelZero(self, twistMessage):
        return twistMessage.linear.x == 0 and twistMessage.angular.x == 0;
    
    def filterCallback(self, twistMessage):
        if self.collisionChecker.barrierDetected:
            twistMessage.linear.x = 0

#            print 'barrierDetected', self.collisionChecker.barrierDetected 
#            print 'twist', twistMessage 

            
            #self.state = _BRAKE
            
#        else:
#            if self.state == _BRAKE and self.cmdVelZero (twistMessage):
#                self.state = _RESEND

#        if self.state == _BRAKE:
#            twistMessage = self.zeroTwistMessage
        
        # rospy.loginfo('state:' + str(self.state) +
        #               ', barrierDetected: ' + str(self.collisionChecker.barrierDetected) +
        #               ', twist: ' + str(twistMessage))
        
        self.cmdVelPub.publish(twistMessage)
        
            
def main():
    rospy.init_node('cmd_vel_filter', anonymous = False)
    collisionChecker = CollisionChecker()
    brakeFilter = BrakeFilter(collisionChecker)
    while not rospy.is_shutdown():
        rospy.spin()
        
if __name__ == '__main__':
    main()

