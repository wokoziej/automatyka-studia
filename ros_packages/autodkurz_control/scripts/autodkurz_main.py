#!/usr/bin/env python

# TODO:
# 1. Timer for Turn class (if something takes too long, we should terminate it - maybe with additional outcome)
# 2. Blind points - when the range sensors do not see barriers:
#  - change positions of sensors?
#  - remeber short historical reads?
# 3. Add some mass to the back of the robot? Avoiding instability of gazebo (mauybe ode?)
import roslib; ### roslib.load_manifest('smach_tutorials')
import rospy
import smach
import smach_ros
import random
from geometry_msgs.msg import Twist
import numpy
from numpy.linalg import norm
import autodkurz_control
import math
import argparse
import sys
from autodkurz_control.barrier_detector import BarrierDetector
from autodkurz_control.positioner import Positioner
import yaml

from std_msgs.msg import Int8
from std_msgs.msg import Float32

g_cmdVelPublisher = False
g_barrierDetector = False
g_twistSetter = False
g_rate = False

class TwistSetter:
    def __init__(self, areaWeightTurnFactor, sensorDistanceTurnFactor):    
        self.twist = Twist()
        self.areaWeightTurnFactor = areaWeightTurnFactor
        self.sensorDistanceTurnFactor = sensorDistanceTurnFactor
        self.twist.linear.x = 0
        self.twist.linear.y = 0
        self.twist.linear.z = 0
        self.twist.angular.x = 0
        self.twist.angular.y = 0
        self.twist.angular.z = 0
        self.speed = 0.3
        self.angularSpeed = 2
        self.left_sonar_dist = 0
        self.right_sonar_dist = 9
        self.right_weight = 0
        self.left_weight = 0
        
        rospy.Subscriber('left_sonar_dist', Float32, self.rememberSonarDistanceL)
        rospy.Subscriber('right_sonar_dist', Float32, self.rememberSonarDistanceR)
        rospy.Subscriber('right_weights', Int8, self.rememberRightWeight)
        rospy.Subscriber('left_weights', Int8, self.rememberLeftWeight)

    def rememberRightWeight(self, weight):
        self.right_weight = weight.data
        
    def rememberLeftWeight(self, weight):        
        self.left_weight = weight.data
        
    def rememberSonarDistanceR(self, msg):
        self.right_sonar_dist = msg.data

    def rememberSonarDistanceL(self, msg):
        self.left_sonar_dist = msg.data

    def speedAccordingToDistance(self):
        minimum = min(self.left_sonar_dist, self.right_sonar_dist)

        # For GAZEBO ->        factor = 2
        factor = 0.5
        
        if minimum > 0.3:
            return 0.75
        else:
            if minimum > 0.17:
                return 0.5 * factor
            else:
                if minimum > 0.13:
                    return 0.1 * factor
                else:
                    return 0.05 * factor
        return minimum
    
    def forward(self):
        self.twist.angular.z = 0.0
        mult = self.speedAccordingToDistance()
               
        #### Test of weight of fields
        if self.areaWeightTurnFactor > 0:
            diff =  self.right_weight - self.left_weight 
            self.twist.angular.z = diff * self.areaWeightTurnFactor

        if self.sensorDistanceTurnFactor > 0 :
            diff =  self.left_sonar_dist - self.right_sonar_dist
            minimum = min(self.left_sonar_dist, self.right_sonar_dist)
            if minimum > 1:
                minimum = 1
            else:
                if minimum < 0.05:
                    minimum = 0 
            self.twist.angular.z += diff * (1 - minimum) * self.sensorDistanceTurnFactor 

        self.twist.linear.x = self.speed * mult;
        return self.twist
    
    def backward(self):
        self.twist.angular.z = 0.0
        self.twist.linear.x = -self.speed * 0.05       
        return self.twist
        
    # def turn(self):
    #     global g_barrierDetector
    #     if g_barrierDetector.collisionSide () != 0:
    #         self.twist.angular.z = -self.angularSpeed
    #     else:
    #         self.twist.angular.z = self.angularSpeed
    #     self.twist.linear.x = 0        
    #     return self.twist

    def calculateTurnSpeed(self, speed, angleToGoal, desiredAngle):
        distance = abs(desiredAngle - angleToGoal)

        factor = 0.5
        if distance > 0.5:
            s = 0.5 * factor 
        else:
            if distance > 0.1:
                s = 0.2 * factor
            else:
                s = 0.1 * factor

        self.twist.angular.z = s * speed        
        self.twist.linear.x = 0        
        return self.twist

    def turnRightTwist(self, angleToGoal, desiredAngle):
        return self.calculateTurnSpeed(-self.angularSpeed, angleToGoal, desiredAngle)
    
    def turnLeftTwist(self, angleToGoal, desiredAngle):
        return self.calculateTurnSpeed(self.angularSpeed, angleToGoal, desiredAngle)
    
class Mover(smach.State):

    def __init__(self, outcomes, input_keys = [], output_keys = [], max_execution_time = sys.maxint, max_execution_runs = -1):
        smach.State.__init__(self, outcomes = outcomes, input_keys = input_keys, output_keys = output_keys)
        self.start_time = 0
        self.execution_count = 0
        self.max_execution_runs = max_execution_runs  # -1 == Infinity
        self.max_execution_duration = rospy.Duration(max_execution_time)

    def timeExpired(self):
        expired = rospy.Time.now() - self.start_time > self.max_execution_duration
        return expired
        
    def calculateTwist(self):
        raise NotImplementedError()

    def getOutcome(self):
        raise NotImplementedError()

    def prepare (self, userdata):
        self.start_time = rospy.Time.now()        
        self.execution_count += 1
#        print 'PREPARE:', self.execution_count, self.start_time, self.max_execution_runs
        
    def finalize(self):
#        print 'FINALIZE:', self.execution_count, rospy.Time.now() 
        if self.executionCountExceeded():
            self.execution_count = 0
                
    def executionCountExceeded(self):
        return self.max_execution_runs >= 0 and self.execution_count > self.max_execution_runs

    def execCountExceededOutcome (self):
        return 'exec_count_exceeded'

    def timeExpiredOutcome (self):
        return 'time_expired'

    def shouldMove(self):
        return not self.timeExpired()
    
    def execute(self, userdata):
        global g_cmdVelPublisher, g_twistSetter, g_rate
        self.prepare(userdata);

        if self.executionCountExceeded():
            self.finalize()
            return self.execCountExceededOutcome () 

        while self.shouldMove ():
            twist = self.calculateTwist() 
            g_cmdVelPublisher.publish( twist )
            g_rate.sleep()
            
        self.finalize()
        if self.timeExpired():
            return self.timeExpiredOutcome ()
        return self.getOutcome()

            
class BarrierOrDistanceAchiever(Mover):
    
    def __init__(self, distance = float("inf"),
                 outcomes = ['exec_count_exceeded', 'time_expired', 'collision', 'distance_achieved'],
                 input_keys = [],
                 output_keys = [],
                 max_execution_runs = -1,
                 max_execution_time = sys.maxint):
        Mover.__init__(self,
                       outcomes = outcomes,
                       input_keys = input_keys,
                       output_keys = output_keys,
                       max_execution_runs = max_execution_runs,
                       max_execution_time = max_execution_time)
        self.position = None;
        self.distance = distance        
        
    def calculateTwist(self):
        global g_twistSetter
        return g_twistSetter.forward()

    def distanceAchieved(self):
        if self.distance == float("inf"):
            return False
        currentPosition = g_positioner.getPosition ()
        dist = numpy.linalg.norm(currentPosition - self.position)
        #    print 'DISTANCE:', self.position, currentPosition, self.distance, dist 

        return dist > self.distance

    def prepare (self, userdata):
         global g_positioner
         Mover.prepare(self, userdata)
         self.position = g_positioner.getPosition()

    def inCollision(self):
        global g_barrierDetector        
        return g_barrierDetector.inCollision()

    def collisionOutcome(self):
        return 'collision'

    def distanceAchievedOutcome(self):
        return 'distance_achieved'
    
    def getOutcome(self):
        if self.inCollision():
            return self.collisionOutcome() 
        return self.distanceAchievedOutcome()

    def shouldMove(self):
        should = ( Mover.shouldMove(self)
                   and not self.inCollision()
                   and not self.distanceAchieved () )
        return should

class MicroStepBack(BarrierOrDistanceAchiever):
    def __init__(self):
        BarrierOrDistanceAchiever.__init__(self,
                                           outcomes = ['last_barrier_on_left', 'last_barrier_on_right', 'time_expired'],
                                           max_execution_time = 5,
                                           distance = 0.03,
                                           input_keys = [],
                                           output_keys = [])
        
    def calculateTwist(self):
        global g_twistSetter
        twist = g_twistSetter.backward()
        #twist.angular.z = random.uniform(-0.05, -0.05)
        return twist;

    def collisionOutcome(self):
        global g_barrierDetector
        return 'last_barrier_on_' + g_barrierDetector.collisionSide ()

    def execCountExceededOutcome (self):
        print '1'
        return 'NONE'

    def distanceAchievedOutcome(self):
        return self.collisionOutcome()

    def shouldMove(self):
        print self.timeExpired(), self.inCollision(), self.distanceAchieved ()
        should = ( (not self.timeExpired() )
                   and
                   ( self.inCollision()
                     or not self.distanceAchieved () ) )
        return should

class GoForward(BarrierOrDistanceAchiever):
    def __init__(self):
        BarrierOrDistanceAchiever.__init__(self,
                                           input_keys = ['counter_in'],
                                           output_keys = ['counter_out'],
                                           outcomes = ['collision', 'distance_achieved'])
        self.const_distance = 0.06
        
    def prepare(self, userdata):
        c = userdata.counter_in + 1
        userdata.counter_out = c
        self.distance = c * self.const_distance
        BarrierOrDistanceAchiever.prepare(self, userdata)


class NewBeginning4Circle(BarrierOrDistanceAchiever):
    def __init__(self, max_execution_runs = -1):
        BarrierOrDistanceAchiever.__init__(self,
                                           input_keys = ['counter_in'],
                                           output_keys = ['counter_out'],
                                           outcomes = [ 'collision', 'distance_achieved', 'exec_count_exceeded'],
                                           max_execution_runs = max_execution_runs)
        self.const_distance = 0.06
        
    def prepare(self, userdata):
        self.distance = max(1.0, (1.75 * userdata.counter_in * self.const_distance))
        userdata.counter_out = 0
        BarrierOrDistanceAchiever.prepare(self, userdata)


class StepForward(BarrierOrDistanceAchiever):
    def __init__(self):
        BarrierOrDistanceAchiever.__init__(self,
                                           outcomes = ['collision', 'distance_achieved'],
                                           distance = 0.07)
            
class Turn(Mover):
    def __init__(self, desiredAngle):
        Mover.__init__(self,
                       outcomes = ['angle_reached'],
                       input_keys = [], output_keys = [])
        self.angle = 0
        self.desiredAngle = desiredAngle
        self.epsilon = 0.01
        self.minMove = self.epsilon * 100
        self.distance = self.desiredAngle

    def getOutcome(self):
        return 'angle_reached'
    
    def prepare (self, userdata):
        global g_positioner
        Mover.prepare(self, userdata)
        self.angle = g_positioner.getYaw ()
        self.distance = self.desiredAngle

    def angleReached (self):
        angleToGoal = self.angleToGoal()
        distance = abs(self.desiredAngle - angleToGoal)
        angleReached = distance < self.epsilon or (
            (distance > self.distance) and not (distance > self.minMove)
        )
        # print angleToGoal, distance, angleReached
        self.distance = distance
        return angleReached 

    def angleToGoal(self):
        yaw = g_positioner.getYaw ()
        return math.pi - abs(math.pi - abs(self.angle - yaw))

    def shouldMove(self):
        should = Mover.shouldMove(self) and not self.angleReached () 
        return should
    
TURN_ANGLE=math.pi / 2.0

class TurnRight(Turn):
    def __init__(self):
        Turn.__init__(self, desiredAngle = TURN_ANGLE)

    def calculateTwist(self):
        global g_twistSetter
        return g_twistSetter.turnRightTwist(self.desiredAngle, self.angleToGoal ())

class TurnLeft(Turn):
    def __init__(self):
        Turn.__init__(self, desiredAngle = TURN_ANGLE)
        
    def calculateTwist(self):
        global g_twistSetter
        return g_twistSetter.turnLeftTwist(self.desiredAngle, self.angleToGoal ())
    
class TurnBack(Turn):
    def __init__(self):
        Turn.__init__(self, desiredAngle = math.pi)
        
    def calculateTwist(self):
        global g_twistSetter
        return g_twistSetter.turnRightTwist(self.desiredAngle, self.angleToGoal ())

class TurnRandom(Turn):
    def __init__(self):
        Turn.__init__(self, desiredAngle = TURN_ANGLE)
        self.flipDirection = 1
        
    def prepare (self, userdata):
        Turn.prepare(self, userdata)
        self.desiredAngle = random.uniform(math.pi/16.0, math.pi/4.0)
        self.flipDirection = random.randint (0, 1)
        self.distance = self.desiredAngle

    def calculateTwist(self):
        global g_twistSetter
        if self.flipDirection == 1:
            return g_twistSetter.turnRightTwist(self.desiredAngle, self.angleToGoal ())
        else:
            return g_twistSetter.turnLeftTwist(self.desiredAngle, self.angleToGoal ())
    
class StrategySelector(smach.State):
    def __init__(self, outcomes):
        self.outcomes = outcomes
        smach.State.__init__(self, outcomes = self.outcomes)
        self.strategy = 0
        
    def execute(self, userdata):
        outcome = self.outcomes [self.strategy % 4]
        self.strategy += 1;
        return outcome 
    

def update_outcomes_and_transitions(config, strategy, strategy_name, outcomes, transitions):
    if  strategy in config:
        outcomes += [strategy_name]
        transitions [strategy_name] = strategy_name

def add_strategy_selector (sm, config):
    outcomes = []
    transitions = {}
    update_outcomes_and_transitions(config, 'parallel_lines_strategy', 'PARALLEL_LINES', outcomes, transitions)
    update_outcomes_and_transitions(config, 'ortogonal_lines_strategy', 'ORTOGONAL_LINES', outcomes, transitions)
    update_outcomes_and_transitions(config, 'circular_lines_strategy', 'CIRCULAR_LINES', outcomes, transitions)    
    update_outcomes_and_transitions(config, 'random_lines_strategy', 'RANDOM_LINES', outcomes, transitions)    
    sm_sub = smach.StateMachine(outcomes=outcomes)
    with sm_sub:
        smach.StateMachine.add('SELECT_STRATEGY', StrategySelector(outcomes), 
                               transitions=transitions,
                               remapping= {})
        
    smach.StateMachine.add('STRATEGY_SELECTOR', sm_sub,
                           transitions=transitions)

def add_parallel_lines_strategy(sm, strategy_cfg):
    sm_sub = smach.StateMachine(outcomes=['strategy_end'])
    with sm_sub:
        smach.StateMachine.add('GO_TO_BARRIER1', BarrierOrDistanceAchiever(max_execution_runs = strategy_cfg.get('max_execution_runs', 1)),
                               transitions = {'collision':'M_STEP_BACK1',
                                              'exec_count_exceeded' : 'strategy_end',
                                              'time_expired' : 'strategy_end',
                                              'distance_achieved' : 'strategy_end'},
                               remapping= {})
            
        smach.StateMachine.add('M_STEP_BACK1', MicroStepBack(), 
                               transitions = {'last_barrier_on_left':'TURN_RIGHT1',
                                              'last_barrier_on_right':'TURN_LEFT1',
                                              'time_expired' : 'strategy_end'}, 
                               remapping= {})
        
        smach.StateMachine.add('TURN_RIGHT1', TurnRight(), 
                               transitions = {'angle_reached':'STEP_FORWARD1'}, 
                               remapping= {})
            
        smach.StateMachine.add('STEP_FORWARD1', StepForward(), 
                               transitions = {'collision':'M_STEP_BACK2',
                                              'distance_achieved' : 'TURN_RIGHT2'},
                               remapping= {})
            
        smach.StateMachine.add('TURN_RIGHT2', TurnRight(), 
                               transitions = {'angle_reached':'GO_TO_BARRIER2'},
                               remapping= {})
            
        smach.StateMachine.add('M_STEP_BACK2', MicroStepBack(), 
                               transitions = {'last_barrier_on_left':'TURN_BACK1',
                                              'last_barrier_on_right':'TURN_BACK1',
                                              'time_expired' : 'strategy_end'},
                               remapping= {})
            
        smach.StateMachine.add('TURN_BACK1', TurnBack(), 
                               transitions = {'angle_reached':'GO_TO_BARRIER1'},
                               remapping= {})
        
        smach.StateMachine.add('TURN_LEFT1', TurnLeft(), 
                               transitions = {'angle_reached':'STEP_FORWARD2'}, 
                               remapping= {})
        
        smach.StateMachine.add('STEP_FORWARD2', StepForward(), 
                               transitions = {'collision':'M_STEP_BACK2',
                                              'distance_achieved' : 'TURN_LEFT2'},
                               remapping= {})
        
        smach.StateMachine.add('TURN_LEFT2', TurnLeft(), 
                               transitions = {'angle_reached':'GO_TO_BARRIER2'},
                               remapping= {})
        
        smach.StateMachine.add('GO_TO_BARRIER2', BarrierOrDistanceAchiever(), 
                               transitions = {'collision' : 'M_STEP_BACK3',
                                              'exec_count_exceeded' : 'strategy_end',
                                              'time_expired' : 'strategy_end',
                                              'distance_achieved' : 'strategy_end'},
                               remapping= {})
        
        smach.StateMachine.add('M_STEP_BACK3', MicroStepBack(), 
                               transitions = {'last_barrier_on_left':'TURN_BACK2',
                                              'last_barrier_on_right':'TURN_BACK2',
                                              'time_expired' : 'strategy_end'}, 
                               remapping= {})
        
        smach.StateMachine.add('TURN_BACK2', TurnBack(), 
                               transitions = {'angle_reached':'GO_TO_BARRIER1'},
                               remapping= {})
    smach.StateMachine.add('PARALLEL_LINES', sm_sub,
                           transitions={'strategy_end':'STRATEGY_SELECTOR'})
        
        
def add_ortogonal_lines_strategy (sm, strategy_cfg):
    sm_sub = smach.StateMachine(outcomes=['strategy_end'])
    with sm_sub:
        smach.StateMachine.add('GO_TO_BARRIER1', BarrierOrDistanceAchiever(max_execution_runs = strategy_cfg.get('max_execution_runs', 1)), 
                               transitions = {'collision':'M_STEP_BACK1',
                                              'exec_count_exceeded' : 'strategy_end',
                                              'time_expired' : 'strategy_end',
                                              'distance_achieved' : 'strategy_end'},
                               remapping= {})
        
        smach.StateMachine.add('M_STEP_BACK1', MicroStepBack(), 
                               transitions = {'last_barrier_on_left':'TURN_RIGHT1',
                                              'last_barrier_on_right':'TURN_LEFT1',
                                              'time_expired' : 'strategy_end'}, 
                               remapping= {})
        
        smach.StateMachine.add('TURN_RIGHT1', TurnRight(), 
                               transitions = {'angle_reached':'GO_TO_BARRIER1'}, 
                               remapping= {})
        
        smach.StateMachine.add('TURN_LEFT1', TurnLeft(), 
                               transitions = {'angle_reached':'GO_TO_BARRIER1'}, 
                               remapping= {})
                
    smach.StateMachine.add('ORTOGONAL_LINES', sm_sub,
                           transitions={'strategy_end':'STRATEGY_SELECTOR'})

def add_circular_strategy (sm, strategy_cfg):
    sm_sub = smach.StateMachine(outcomes=['strategy_end'])
    sm_sub.userdata.circular_step_counter = 0;

    with sm_sub:
        smach.StateMachine.add('C_NEW_START', NewBeginning4Circle(strategy_cfg.get('max_execution_runs', 1)), 
                               transitions = {'collision':'START_NEW_CIRCLE',
                                              'distance_achieved':'GO_FORWARD1',
                                              'exec_count_exceeded' : 'strategy_end'},
                               remapping={'counter_in':'circular_step_counter', 
                                          'counter_out':'circular_step_counter'})
        
        smach.StateMachine.add('GO_FORWARD1', GoForward(), 
                               transitions = {'collision':'START_NEW_CIRCLE',
                                              'distance_achieved':'C_TURN_RIGHT1'},
                               remapping = {'counter_in':'circular_step_counter', 
                                            'counter_out':'circular_step_counter'})

        smach.StateMachine.add('C_TURN_RIGHT1', TurnRight(), 
                               transitions = {'angle_reached':'GO_FORWARD2'}, 
                               remapping= {})
        
        smach.StateMachine.add('GO_FORWARD2', GoForward(), 
                               transitions = {'collision':'START_NEW_CIRCLE',
                                              'distance_achieved':'C_TURN_RIGHT2'},
                               remapping = {'counter_in':'circular_step_counter', 
                                            'counter_out':'circular_step_counter'})
        
        smach.StateMachine.add('C_TURN_RIGHT2', TurnRight(), 
                               transitions = {'angle_reached':'GO_FORWARD1'}, 
                               remapping= {})

        
        smach.StateMachine.add('START_NEW_CIRCLE', MicroStepBack(), 
                               transitions = {'last_barrier_on_left':'C_TURN_RIGHT3',
                                              'last_barrier_on_right':'C_TURN_LEFT1',
                                              'time_expired':'strategy_end'}, 
                               remapping= {})
        
        
        smach.StateMachine.add('C_TURN_LEFT1', TurnLeft(), 
                               transitions = {'angle_reached':'C_NEW_START'}, 
                               remapping= {})

        smach.StateMachine.add('C_TURN_RIGHT3', TurnRight(), 
                               transitions = {'angle_reached':'C_NEW_START'}, 
                               remapping= {})
 
        
    smach.StateMachine.add('CIRCULAR_LINES', sm_sub,
                           transitions={'strategy_end':'STRATEGY_SELECTOR'}) 
    
def add_random_strategy (sm, strategy_cfg):
    sm_sub = smach.StateMachine(outcomes=['strategy_end'])
    with sm_sub:
        smach.StateMachine.add('GO_TO_BARRIER1', BarrierOrDistanceAchiever(max_execution_runs = strategy_cfg.get('max_execution_runs', 1)), 
                               transitions = {'collision':'M_STEP_BACK1',
                                              'exec_count_exceeded' : 'strategy_end',
                                              'time_expired' : 'strategy_end',
                                              'distance_achieved' : 'strategy_end'},
                               remapping= {})
        
        smach.StateMachine.add('M_STEP_BACK1', MicroStepBack(), 
                               transitions = {'last_barrier_on_left':'TURN_RANDOM',
                                              'last_barrier_on_right':'TURN_RANDOM',
                                              'time_expired' : 'strategy_end'}, 
                               remapping= {})
        
        smach.StateMachine.add('TURN_RANDOM', TurnRandom(), 
                               transitions = {'angle_reached':'GO_TO_BARRIER1'}, 
                               remapping= {})
                
    smach.StateMachine.add('RANDOM_LINES', sm_sub,
                           transitions={'strategy_end':'STRATEGY_SELECTOR'})

    
def main(config):
    global g_barrierDetector
    global g_cmdVelPublisher 
    global g_twistSetter
    global g_rate
    global g_positioner
    
    rospy.init_node('autodkurz_main_sm')
    g_rate = rospy.Rate(30) # 1hz
    g_barrierDetector = BarrierDetector()
    g_twistSetter = TwistSetter(areaWeightTurnFactor = config.get('area_weight_turn_factor', False),
                                sensorDistanceTurnFactor = config.get('sensor_distance_turn_factor', False))
    g_cmdVelPublisher = rospy.Publisher('/auto/cmd_vel', Twist, queue_size=1)
    g_positioner = Positioner()

    # Create a SMACH state machine
    sm = smach.StateMachine(outcomes=[])
    
    # Open the container
    with sm:
        add_strategy_selector(sm, config)
        if 'parallel_lines_strategy' in config:
            add_parallel_lines_strategy(sm, config['parallel_lines_strategy'])
        if 'ortogonal_lines_strategy' in config:          
            add_ortogonal_lines_strategy (sm, config['ortogonal_lines_strategy'])
        if 'circular_lines_strategy' in config:            
            add_circular_strategy (sm, config['circular_lines_strategy'])
        if 'random_lines_strategy' in config:            
            add_random_strategy (sm, config['random_lines_strategy'])
        
    # Create and start the introspection server
    sis = smach_ros.IntrospectionServer('introspection_server', sm, '/SM_ROOT')
    sis.start()

    # Execute SMACH plan
    outcome = sm.execute()
    rospy.spin()
    sis.stop()




parser = argparse.ArgumentParser()
parser.add_argument("--config-file",
                    help="Config file path",
                    default="config.yaml")
    
if __name__ == '__main__':
    args, unknown = parser.parse_known_args()
    try:
        config = yaml.safe_load(open(args.config_file))
    except:
        config = { "area_weights_turn_factor" : 0.0,
                   "sensor_distance_turn_factor" : 2.0,
                   "parallel_lines_strategy" : { "max_execution_runs": 0 },
                   "ortogonal_lines_strategy" : { "max_execution_runs": 1 },
                   "circular_lines_strategy"  : { "max_execution_runs" : 0 },
                   "random_lines_strategy"  : { "max_execution_runs" : 0 } }
    main(config)
