#!/usr/bin/env python
import random
import argparse
import subprocess 

import rospy, sys
from geometry_msgs.msg import Twist


parser = argparse.ArgumentParser()
parser.add_argument("--num_of_bots", help="Number of robots",
                    type=int)
parser.add_argument("--area_radius", help="Radious of area where robots will be spawned",
                    type=int)

if __name__=="__main__":
    args = parser.parse_args()
    bots_cnt = args.num_of_bots
    area_radius = args.area_radius
    rospy.init_node('teleop', anonymous=True)
    rate = rospy.Rate(1) # 1hz
    publishers = []
    for bot_num in range(0, bots_cnt):
        # Spawn
        x = random.uniform(-area_radius, area_radius)
        y = random.uniform(-area_radius, area_radius)
        bot_name = "bot" + str(bot_num)
        subprocess.call(["roslaunch",
                         "autodkurz_gazebo",
                         "autodkurz_spawner.launch",
                         "model_name:=" + bot_name,
                         "x:="+str(x),
                         "y:="+str(y)])

        pub = rospy.Publisher("/" + bot_name + "/cmd_vel", Twist, queue_size=5)
        publishers.append(pub)

    while not rospy.is_shutdown():
        for i, pub in enumerate(publishers):            
            control_speed = random.randint(-1, 1) * random.random()
            control_turn = random.randint(-1, 1) * random.random()
            twist = Twist()
            twist.linear.x = control_speed;
            twist.linear.y = 0;
            twist.linear.z = 0
            twist.angular.x = 0;
            twist.angular.y = 0;
            twist.angular.z = control_turn
            pub.publish(twist)
        rate.sleep()
        
