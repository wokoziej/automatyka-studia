#!/usr/bin/env python

# TOPICS TO SERIAL
# /left_wheel_speed -> [L]eft[D]irection[SPEED]
# /right_wheel_speed -> [R]ight[D]irection[SPEED]
# Direction: [F]orward|[B]ackward|[S]top
# Vacuum on / off : V(0/1)
# Distance sensors turning on/off: D(0/1)
# Examples:
#  Left wheel forward with speed 125 = LF125
#  Right wheel backward with speed 200 = RB200

# SERIAL TO TOPICS
# Serial message format:
#    turn [radians] | switches [byte flags]| left ticks [integer] | right ticks [integer] | left distance [mm] | right distance [mm]
# turn and ticks gives us odometry messages in ROS
# switches inform us about barriers detctions
# and distance comes from sonar sensors (minimal value from all sonars)
# Examples:
#  T<REAL_NUMER>S<6xBYTE>L<INTEGER>R<INTEGER>r<BYTE>l<BYTE>

import roslib
import rospy
import serial
from std_msgs.msg import Int8
from std_msgs.msg import Int16

from std_msgs.msg import Float32
from threading import Thread, Lock
import re
import autodkurz_control
from autodkurz_control.msg import LimitSwitchState
import argparse

parser = argparse.ArgumentParser()
parser.add_argument("--port", help="Serial port path",
                    default="/dev/ttyACM0")
parser.add_argument("--baud", help="Baud rate",
                    type=int,
                    default = 115200)
_DESTINATION_TOPIC_NAME = "/switches"


class SpeedToSerial:
    def __init__(self, port, baud):
        rospy.Subscriber('/left_wheel_speed', Int8, self.leftSpeedChanged, queue_size=1)
        rospy.Subscriber('/right_wheel_speed', Int8, self.rightSpeedChanged, queue_size=1)
        self.lWheelPub = rospy.Publisher('lwheel', Int16, queue_size=10)
        self.rWheelPub = rospy.Publisher('rwheel', Int16, queue_size=10)        
        self.lSonarDistPub = rospy.Publisher('left_sonar_dist', Float32, queue_size=10)
        self.rSonarDistPub = rospy.Publisher('right_sonar_dist', Float32, queue_size=10)        
        self.switchesPub = rospy.Publisher(_DESTINATION_TOPIC_NAME,
                                           autodkurz_control.msg.LimitSwitchState,
                                           queue_size = 10)    
        self.ser = serial.Serial(port, baud)
        self.ser.timeout = 0
        self.message = ""
        self.switchesMem = [-1,-1,-1,-1,-1,-1]

    def leftSpeedChanged (self, speed):
        self.sendNewWheelSpeed ('L', speed.data)

    def rightSpeedChanged (self, speed):
        self.sendNewWheelSpeed ('R', speed.data)
    
    def sendNewWheelSpeed (self, wheelSide, speed):
        wheelDirection = "F" if speed > 0 else "B" if speed < 0 else 'S'        
        speedVal = min(255, abs(speed) * 2)
        message = "{}{}{}\n".format(wheelSide, wheelDirection, speedVal)
        written = self.ser.write(message)
        rospy.loginfo("bytes written = {}, message = '{}'".format(written, message))
        self.ser.flush()

    def publishChangedSwitches(self, switches):
        cnt = 0
        for i in switches:
            currentState = int(i)
            if currentState != self.switchesMem[cnt]:
                self.switchesMem[cnt] = currentState
                switch = LimitSwitchState()
                switch.open = currentState
                switch.switch_id = cnt 
                self.switchesPub.publish(switch)
                print "publish: ", switch

            cnt += 1
                         
    def parseMessageAndPublish(self, message):
        TURN = "T"
        SWITCHES = "S"
        LEFT_TICKS = "L"
        RIGHT_TICKS = "R"
        LEFT_DISTANCE = "l"
        RIGHT_DISTANCE = "r"
        
        keys = ["?", TURN, SWITCHES, LEFT_TICKS, RIGHT_TICKS, LEFT_DISTANCE, RIGHT_DISTANCE]

        regexp = "[{}]".format("".join (keys))
        splitted = re.split(regexp, message)

        
        if len(splitted) != len(keys):
            rospy.logerr("Incomplete message: {}".format(message))
        else:
            turn = splitted [keys.index(TURN)]
            switches = splitted [keys.index(SWITCHES)]
            self.publishChangedSwitches(switches)
            
            left_ticks = Int16()
            left_ticks.data = int(splitted [keys.index(LEFT_TICKS)])
            right_ticks = Int16()
            right_ticks.data = int(splitted [keys.index(RIGHT_TICKS)])
            self.lWheelPub.publish(left_ticks)
            self.rWheelPub.publish(right_ticks)
            
            ldistance = float(splitted [keys.index(LEFT_DISTANCE)]) / 1000.0
            rdistance = float(splitted [keys.index(RIGHT_DISTANCE)]) / 1000.0
            self.lSonarDistPub.publish(ldistance)
            self.rSonarDistPub.publish(rdistance)
            
        
    def readMessage(self):
        buf = self.ser.read(256);
        message_len = len(buf)
        if message_len > 0:
            self.message += buf
            while True:                
                m2 = self.message.partition("\n")[2]                
                if len(m2) == 0:
                    break;
                else:
                    m0 = self.message.partition("\n")[0]
                    m0 = m0.replace("\r", "")
                    #rospy.loginfo("< message = {}".format(m0))
                    self.parseMessageAndPublish(m0)
                    self.message = m2
        
def main(args):
    
    port = args.port
    baud = args.baud
    
    rospy.init_node('ros_to_serial', anonymous = False)   
    rate = rospy.Rate(10) # 10hz
    speedToSerial = SpeedToSerial(port, baud)
    while not rospy.is_shutdown():
        speedToSerial.readMessage();
        rate.sleep()

if __name__ == '__main__':
    args, unknown = parser.parse_known_args()
    main(args)

    
