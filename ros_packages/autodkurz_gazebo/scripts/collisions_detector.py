#!/usr/bin/env python


#
# Script concert gazebo collision messages to switches messages for collision informatio simulation
#


#import geometry_msgs;
#from geometry_msgs.msg import Twist
#import message_filters;
#from message_filters import Subscriber
import gazebo_msgs
import roslib;
import rospy
import autodkurz_control
from autodkurz_control.msg import LimitSwitchState
from gazebo_msgs.msg import ContactsState
_COLLISION_TOPIC_NAME = "/autodkurz/contacts"
_DESTINATION_TOPIC_NAME = "/switches"

_COLLISION_NAMES = ["autodkurz::base_footprint::base_footprint_fixed_joint_lump__rf_bumper_collision_6",
	            "autodkurz::base_footprint::base_footprint_fixed_joint_lump__rsf_bumper_collision_8",
	            "autodkurz::base_footprint::base_footprint_fixed_joint_lump__rsb_bumper_collision_7",
	            "autodkurz::base_footprint::base_footprint_fixed_joint_lump__lf_bumper_collision_3",
	            "autodkurz::base_footprint::base_footprint_fixed_joint_lump__lsf_bumper_collision_5",
	            "autodkurz::base_footprint::base_footprint_fixed_joint_lump__lsb_bumper_collision_4" ]

_FRAME_NAMES = ["rf_bumper", "rsf_bumper", "rsb_bumper", "lf_bumper", "lsf_bumper", "lsb_bumper" ]

class CollisionTranslator:
    def __init__(self):
        self.collisionSub = rospy.Subscriber(_COLLISION_TOPIC_NAME,
                                             gazebo_msgs.msg.ContactsState,
                                             self.statesChanged)
        self.switchesPub = rospy.Publisher(_DESTINATION_TOPIC_NAME,
                                           autodkurz_control.msg.LimitSwitchState,
                                           queue_size = 10)    

    def calculateSwitchPosition(self, collName):
        switch = LimitSwitchState()
        switch.open = False
        switch.switch_id = _COLLISION_NAMES.index(collName)
        return switch
    
    def statesChanged (self, contacts):
#        print ' ========== ', contacts
        
        try:
            if len(contacts.states) > 0:
                for state in contacts.states:
                    for i in _COLLISION_NAMES:
                        if i in [state.collision1_name, state.collision2_name]:
                            try:
                                switch = self.calculateSwitchPosition(i)                  
                                self.switchesPub.publish(switch)
                            except Exception as e:
                                print "CollisionDetector", e
                                print ':', i
                                print '-----'
                                print state
                                print '-----'
                                print '>-----'
                                print state.contact_positions
                                print '>-----'
                                
            else:
                if contacts.header.frame_id in _FRAME_NAMES:
                    switch = LimitSwitchState()
                    switch.open = True
                    switch.switch_id = _FRAME_NAMES.index(contacts.header.frame_id)
                    self.switchesPub.publish(switch)
        except Exception as e: print(e)                    

            
def main():
    rospy.init_node('collision_detector', anonymous = False)
    collisionChecker = CollisionTranslator()
    while not rospy.is_shutdown():
        rospy.spin()
        
if __name__ == '__main__':
    main()

