#!/usr/bin/env python

import roslib;
import rospy
from sensor_msgs.msg import Range
from std_msgs.msg import Float32

class Range2DistanceConverter:
    def __init__(self):

        rospy.Subscriber('sonar_l', Range, self.publishLDistance)
        rospy.Subscriber('sonar_r', Range, self.publishRDistance)
        self.lDistPub = rospy.Publisher('left_sonar_dist', Float32, queue_size = 8)    
        self.rDistPub = rospy.Publisher('right_sonar_dist', Float32, queue_size = 8)    
        
    def publishLDistance (self, msg):
        self.lDistPub.publish(msg.range);

    def publishRDistance (self, msg):
        self.rDistPub.publish(msg.range);
            
def main():
    rospy.init_node('range_2_distance', anonymous = False)
    range2DistanceConverter = Range2DistanceConverter()
    while not rospy.is_shutdown():
        rospy.spin()
        
if __name__ == '__main__':
    main()

