#!/usr/bin/env python
import argparse
import random
import datetime
import rospy
from move_base_msgs.msg import MoveBaseAction, MoveBaseGoal
import actionlib
from actionlib_msgs.msg import *
import tf.transformations;

from tf import TransformListener

# t = tf.Transformer(True, rospy.Duration(10.0))
# m = geometry_msgs.msg.TransformStamped()
# m.header.frame_id = "THISFRAME"
# m.parent_id = "PARENT"
# t.setTransform(m)


parser = argparse.ArgumentParser()
parser.add_argument("--area_radius",
                    help="Radious of area to explore (starting from robot position)",
                    type=int,
                    default=7)

parser.add_argument("--base_frame_id",
                    help="Id of robot base",
                    default="/base_footprint")

parser.add_argument("--map_frame_id",
                    help="Id of robot base",
                    default="/odom")

parser.add_argument("--time_to_wait_for_results",
                    help="How many seconds should we wait for getting results from move base",
                    type=int,
                    default=120)

parser.add_argument("--no_of_points_to_visit",
                    help="How many seconds should we wait for getting results from move base",
                    type=int,
                    default=10)

parser.add_argument("--strategy",
                    help="Exploration strategy: random - choose points coordination randomly, grid - map points as grid",
                    default="random",
                    choices=["random"])

class Explorer():
    def __init__(self, args):
        rospy.init_node('explore', anonymous=False)
	rospy.on_shutdown(self.shutdown)
	self.area_radius = args.area_radius
	self.base_frame_id = args.base_frame_id
        self.time_to_wait_for_results = args.time_to_wait_for_results
        self.map_frame_id = args.map_frame_id
        self.no_of_points_to_visit = args.no_of_points_to_visit
        self.strategy = args.strategy
        self.step = 0.3;
        
	self.move_base = actionlib.SimpleActionClient("move_base", MoveBaseAction)
	rospy.loginfo("waiting for the action server to come up")
	self.move_base.wait_for_server(rospy.Duration(5))
        self.tf = TransformListener()        

    def next_target_defined(self):
        if self.strategy == "random":
            try:
                t = self.tf.getLatestCommonTime(self.base_frame_id, self.map_frame_id)
                position, quaternion = self.tf.lookupTransform(self.map_frame_id, self.base_frame_id, t)                
                now = rospy.Time.now();                
                lastMoveTime = self.goal.target_pose.header.stamp;
                if abs(lastMoveTime - now) < rospy.Duration(1):
                    stepMultiplier = self.step * 3
                else:
                    stepMultiplier = self.step;
                self.goal.target_pose.header.stamp = now

                self.direction = 1
                x = position [0] + self.direction * stepMultiplier;
                y = position [1];

                self.goal.target_pose.pose.position.x = x
                self.goal.target_pose.pose.position.y = y
                
                angle = 0.14
                axis = [0, 0, 1]
                q = tf.transformations.quaternion_about_axis(angle, axis);

                self.goal.target_pose.pose.orientation.x = q[0]
                self.goal.target_pose.pose.orientation.y = q[1]
                self.goal.target_pose.pose.orientation.z = q[2]
                self.goal.target_pose.pose.orientation.w = q[3]

                return 1
            except Exception, e:
                rospy.loginfo('E: '+ str(e))
            
        return 0
    
    def move_to_target(self):
        rospy.loginfo("Going to:\n" + str(self.goal.target_pose.pose.position))
        self.move_base.send_goal(self.goal)
	success = self.move_base.wait_for_result(rospy.Duration(self.time_to_wait_for_results)) 
        if not success:
            self.move_base.cancel_goal()
            rospy.loginfo("Goal not reached. Canceled.")
    	else:
	    self.state = self.move_base.get_state()
	    if self.state == GoalStatus.SUCCEEDED:
		rospy.loginfo("Goal reached!!!")
            else:
		rospy.loginfo("Success but not SUCCEEDED?")
                
    def explore(self):
        rospy.loginfo("exploration started")
        self.state = 0
	self.goal = MoveBaseGoal()
        self.goal.target_pose.header.frame_id = self.map_frame_id
        no_of_visited_points = 0;
        while no_of_visited_points != self.no_of_points_to_visit:
            transformExists = 0
            # Define next target            
            if self.next_target_defined():             
                # Move to this target
                self.move_to_target()
                no_of_visited_points += 1;

    def shutdown(self):
        rospy.loginfo("Stop")


if __name__ == '__main__':
    try:
        args = parser.parse_args()
        explorer = Explorer (args)
        explorer.explore()
        
    except rospy.ROSInterruptException:
        rospy.loginfo("Exception thrown")

