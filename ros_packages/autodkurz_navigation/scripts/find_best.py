import numpy as np
import yaml

directory = "/media/wojtas/32986DF9986DBC4B/hause2_full2"
resfilename = "{}/results.txt".format(directory)
    
def prepare_summary_results():
    results = {}
    for p in range(0, 2):
        for o in range(0, 2):  
            for c in range (0, 2):
                for r in range (0, 2):
                    for w in range (0, 2):
                        weight_factor = w / 2.0
                        for f in range(0, 7):  
                            distance_factor = f / 2.0                            
                            if (p != 0 or o != 0 or c !=0 or r != 0):
                                for i in range(0, 4):                    
                                    name = "o{}_p{}_c{}_r{}_wf_{}_df_{}".format(o, p, c, r, weight_factor, distance_factor)
                                    sub = "{}_range_i_{}".format(name, i)
                                    path = "{}/{}".format(directory, sub)
                                    fn = "{}/mean.txt".format(path)
                                    stddev = None
                                    try:
                                        mean = np.loadtxt(fn)
                                        stddev = np.loadtxt("{}/stddev.txt".format(path))                                                                                                                    
                                    except:
                                        print "No file"

                                    if stddev is not None:
                                        no_of_last = 10
                                        last_greates_ind = np.argsort(mean)[-no_of_last:]
                                        last_greatest_mean = np.zeros(no_of_last)
                                        last_stddev = np.zeros(no_of_last)
#                                        print last_stddev
#                                        exit (0)
                                        for i in range(no_of_last):
                                            last_greatest_mean[i] = mean[last_greates_ind[i]]
                                            last_stddev[i] = stddev[last_greates_ind[i]]
                                            
                                        results[sub] = { "mean" : last_greatest_mean.tolist(),
                                                         "stddev" : last_stddev.tolist()}

                                        
    with open(resfilename, 'w') as outfile:
        yaml.dump(results, outfile, default_flow_style=False)
#                                    print results
#                                    exit(0)
                                   

if __name__ == '__main__':
#    prepare_summary_results()
    results  = yaml.safe_load(open(resfilename))
    rsort = {}
    for k, v in results.iteritems():
        rsort[k] = max(v['mean'])
        
    rsorted = sorted(rsort, key=rsort.get)

    for k in rsorted[-20:]:
        print "{} : {}, {}".format(k, results[k]['mean'][:3], results[k]['stddev'][:1])



                                        
