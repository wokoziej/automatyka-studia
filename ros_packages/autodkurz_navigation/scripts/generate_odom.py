#!/usr/bin/env python

import math
from math import sin, cos, pi

import rospy
import tf
from nav_msgs.msg import Odometry
from geometry_msgs.msg import Point, Pose, Quaternion, Twist, Vector3

from gazebo_msgs.srv import * 
import tf
import rospy
from math import radians

rospy.init_node('odometry_publisher')

odom_pub = rospy.Publisher("odom", Odometry, queue_size=50)
odom_broadcaster = tf.TransformBroadcaster()

current_time = rospy.Time.now()
last_time = rospy.Time.now()
r = rospy.Rate(10.0)
service = '/gazebo/get_link_state' 
rospy.wait_for_service(service)

while not rospy.is_shutdown():
    current_time = rospy.Time.now()
    get_link_state = rospy.ServiceProxy(service, GetLinkState)
    resp = get_link_state('base_footprint', '')
    o = resp.link_state.pose.orientation;
    p = resp.link_state.pose.position;    

    # first, we'll publish the transform over tf
    odom_broadcaster.sendTransform( (p.x, p.y, p.z), (o.x, o.y, o.z, o.w),
                                    current_time, "base_footprint", "odom")

    # next, we'll publish the odometry message over ROS
    odom = Odometry()
    odom.header.stamp = current_time
    odom.header.frame_id = "odom"

    # set the position
    odom.pose.pose = Pose(Point(p.x, p.y, p.z), o)

    # set the velocity
    odom.child_frame_id = "/base_footprint"
    odom.twist.twist = Twist(resp.link_state.twist.linear, resp.link_state.twist.angular)

    # publish the message
    odom_pub.publish(odom)
    last_time = current_time
    r.sleep()
