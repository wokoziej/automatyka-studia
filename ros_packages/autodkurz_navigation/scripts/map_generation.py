#!/usr/bin/env python

import math
import numpy as np
from math import sin, cos, pi
import random
import rospy
import tf
from std_msgs.msg import Int8
from std_msgs.msg import Float64
from nav_msgs.msg import Odometry
from nav_msgs.msg import OccupancyGrid
from map_msgs.msg import OccupancyGridUpdate
from tf import TransformListener
import autodkurz_control
from geometry_msgs.msg import Point, Pose, Quaternion, Twist, Vector3

from autodkurz_control.barrier_detector import BarrierDetector
from autodkurz_control.positioner import Positioner

g_positioner = Positioner()
    
rospy.init_node('map_publisher')
odom_broadcaster = tf.TransformBroadcaster()
last_time = rospy.Time.now()
current_time = rospy.Time.now()

# compute odometry in a typical way given the velocities of the robot
# next, we'll publish the odometry message over ROS
RESOLUTION = 0.02

#x = -RESOLUTION / 2
#y = -RESOLUTION / 2

MAP_WIDTH= int(15/RESOLUTION)
MAP_HEIGHT= int(20/RESOLUTION)

fPublisher  = rospy.Publisher("fun", Float64, queue_size=10)

OB_RESOLUTION = RESOLUTION
OB_MAP_WIDTH = 256 # int(MAP_WIDTH/8)
OB_MAP_HEIGHT = OB_MAP_WIDTH

W_MAP_WIDTH = 62 
W_MAP_HEIGHT = W_MAP_WIDTH


ob = []
for i in range(0, OB_MAP_WIDTH):
    for j in range(0, OB_MAP_HEIGHT):
        ob.append(0)

class Mapper:
    def __init__(self, width, height, resolution, topic):
        self.width = width
        self.height = height
        self.resolution = resolution
        self.map = OccupancyGrid ();
        self.map.header.frame_id = "map"
        self.map.info.map_load_time = current_time
        self.map.info.resolution = resolution
        self.map.info.width = width
        self.map.info.height = height
        mapOrigin = Pose()
        mapOrigin.position.x = -( width * resolution) / 2.0 
        mapOrigin.position.y = -( height * resolution) / 2.0
        self.map.info.origin = mapOrigin
        self.publisher = rospy.Publisher(topic, OccupancyGrid, queue_size=10)
        self.data = []
        self.reset()

    def reset (self):
        self.data = []
        for i in range(0, self.width):
            for j in range(0, self.height):
                self.data.append (0)

    def rotate(self, origin, point, angle):
        """
        Rotate a point counterclockwise by a given angle around a given origin.
        
        The angle should be given in radians.
        """
        ox, oy = origin
        px, py = point
        d_px_ox = px - ox
        d_py_oy = py - oy
    
        ca = math.cos(angle)
        sa = math.sin(angle)
        qx = ox + ca * d_px_ox - sa * d_py_oy
        qy = oy + sa * d_px_ox + ca * d_py_oy
        return int(qx), int(qy)
    
    def markDataFromDict (self, dictionary, x, y):
        index = self.index(x, y)
        if index in dictionary:
            dictionary[index] += 1 
        else:
            dictionary[index] = 1
        self.data [index] = min(126, dictionary[index]) 
                
    def valueOf(self, x, y):
        return self.data [self.index(x, y)]
    
    def index(self, x, y):
        return (self.width * x) + y

    def localIndex(self, x, y):
        return self.index(self.width / 2 + x, self.height / 2 + y)    

    def updateData (self, x, y, val):
        i = self.localIndex(x, y)
        self.data [i] = val

    def publishMap(self):
        odom = Odometry()
        self.map.header.stamp = current_time
        self.map.data = self.data;
        odom_quat = tf.transformations.quaternion_from_euler(0, 0, 0)
        odom_broadcaster.sendTransform(
            (-self.resolution/2, -self.resolution/2, 0.),
            odom_quat,
            current_time,
            "map",
            "odom"
        )    
        self.publisher.publish(self.map)

    def traverseRecStart(self, x, y, o):
        return
    
    def traverseRecEnd(self, x, y, o):
        return
    
    def traverseRec(self, l, x_coord, y_coord, x, y, o, firstRun = True):
        if firstRun:
            self.traverseRecStart(x_coord + x, y_coord + y, o)
            
        if l > 1:
            for c in range(1, l + 1 ):
                self.traverseSquare(c, 1, x_coord + x, y_coord + y, o, False)
        else:
            o.f(x_coord + x, y_coord + y)
            
        if firstRun:
            self.traverseRecEnd(x_coord + x, y_coord + y, o)
                
    def traverseSquare(self, r, l, x, y, o, firstRun = True):
        if r == 1:
            if firstRun:
                self.traverseRecStart(x, y, o)                
            o.f(x, y)
            if firstRun:
                self.traverseRecEnd(x, y, o)                
                
        else:
            w = 2 * l - 1
            y_coord = - r + 1 + y
            for x_coord in range(-r + w + 1, r, w):
                self.traverseRec(l, x_coord, y_coord, x, 0, o, firstRun)
                    
            y_coord = r - 1 + y
            for x_coord in range(-r + 1, r - 1, w):
                self.traverseRec(l, x_coord, y_coord, x, 0, o, firstRun)

            x_coord = r - 1 + x                
            for y_coord in range(-r + w + 1, r, w):
                self.traverseRec(l, x_coord, y_coord, 0, y, o, firstRun)

            x_coord = - r + 1 + x
            for y_coord in range(-r + 1, r - 1, w):
                self.traverseRec(l, x_coord, y_coord, 0, y, o, firstRun)


class Sumator:

    def __init__(self, mapper):
        self.mapper = mapper
        self.update_x = 0
        self.update_y = 0
        self.sum = 0
        self.counter = 0
        
    def start(self, sx, sy, x, y):
        self.sum = 0
        self.counter = 0
        self.update_x = sx
        self.update_y = sy

    def f(self, x, y):
        v = self.component(x, y)
        self.sum += v
        self.counter += 1

    def component(self, x, y):
        return self.mapper.valueOf(x + self.update_x, y + self.update_y)
        
    def end(self, x, y, obMapper):
        i = obMapper.index(x, y)
        obMapper.sqrObserved[i] = self.sum / self.counter


class Visualizer:

    def __init__(self, obMapper):
        self.obMapper = obMapper
        self.val = 0
        
    def start(self, sx, sy, x, y):
        self.val =  self.retrieve(x, y)
                
    def f(self, x, y):        
        li = self.obMapper.localIndex(x, y)
        self.obMapper.data[li] = self.val

    def end(self, x, y, obMapper):
        return

    def retrieve(self, x, y):
        i = self.obMapper.index(x, y)
        val = 0
        if i in self.obMapper.sqrObserved:
            val = min(126, self.obMapper.sqrObserved [i])
        return val
    
    def f(self, x, y):        
        li = self.obMapper.localIndex(x, y)
        self.obMapper.data[li] = self.val

    
class ObservationMapper(Mapper):

    def __init__(self, width, height, resolution, topic, sumator):
        Mapper.__init__(self, width, height, resolution, topic)
        self.sumator = sumator 
        self.visualizer = Visualizer(self)        
        self.currentVal = 0
        self.currentCounter = 0
        self.update_x = 0
        self.update_y = 0
        self.sqrObserved = {}        

    
    def traverseRecStart(self, x, y, o):
        o.start(self.update_x, self.update_y, x, y)
    
    def traverseRecEnd(self, x, y, o):
        o.end(x, y, self)
    
    ##### Only tests
    def start(self, sx, sy, x, y):
        print "--", x, y
        return
    def f(self, x, y):
        print x, y
        self.data[self.localIndex(x, y)] = 10
    def end(self, x, y, obMapper):
        return
    ##### Only tests
    
    def updateObMap(self, robotx, roboty, g_x, g_y):

        self.update_x = g_x
        self.update_y = g_y
        self.map.info.origin.position.x = robotx - (self.width * self.resolution)/2.0 
        self.map.info.origin.position.y = roboty - (self.height * self.resolution)/2.0
        x = 1
        y = 1
        
        self.reset()
        self.sqrObserved = {}        

        a = [.2, .2, .2, .2, .2]
        squareRadius = self.width/2 - 2*len(a)
        self.calculate(squareRadius, a)        
        self.visualise(squareRadius, a)
        #        self.printObserved()
        
    def printObserved(self):
        c = 0
        l = ""
        h = int(2.5*math.sqrt ( len(self.sqrObserved) ))
        print "OBSERVED (compressed squeres)", h
        for i in self.sqrObserved.values():
            c += 1
            l += chr(i+32)
            if c == h:
                print l
                c = 0
                l = ""

    def calculate(self, R, a):
        self.mapSquareAccordingTo(R, a, 0, 0, self.sumator)

    def visualise(self, R, a):
        self.mapSquareAccordingTo(R, a, 0, 0, self.visualizer)

    def mapSquareAccordingTo(self, R, a, x, y, o):
        la = len(a)    
        l = 1
        g = int(a [l - 1] * R)
        r = 1
        w = 1
        pr = 0
        while r < g:
            self.mapSquare(r, l, x, y, o)
            pr = r
            r += w

        while l <= la:            
            if r + l - 1 > R:
                break
            
            nl = l + 1
            nw = 2 * nl - 1
            s = int ( r / nw )            
            nr = r 
            dl = 2 * (pr - 1) + w
            while  nr - nl + 1 <= pr + l - 1  or (dl % nw != 0):
                nr += 1 # s * nw + 1
                while r <= nr - nl:
                    self.mapSquare(r, l, x, y, o)
                    pr = r
                    r += w
                    dl = 2 * (pr - 1) + w
                    
            self.mapSquare(nr, nl, x, y, o)
            l = nl
            w = nw
            r = nr            
            pr = r
            r += w

            if l <= la:
                g += int(a [l - 1] * R)
            while r < g:
                self.mapSquare(r, l, x, y, o)
                pr = r
                r += w                
    def mapSquare(self, r, l, x, y, o):
        if r + l <= self.width/2:
            self.traverseSquare (r, l, x, y, o)
            
            

        
class VisitMapper(Mapper):
    
    def __init__(self, width, height, resolution, topic, base_radius):
        self.base_radius = base_radius
        Mapper.__init__(self, width, height, resolution, topic)
        self.visited = {}
    
    def markVisitAt (self, x, y):
        for i in range(-self.base_radius, self.base_radius + 1):
            for j in range(-self.base_radius, self.base_radius + 1):
                self.markDataFromDict (self.visited, x + i, y + j)

    def f(self):
        p = 0.0
        for z in self.visited.itervalues():
            p += 1.0 / z
        return p

class BarrierMapper(Mapper):

    def __init__(self, width, height, resolution, topic, base_radius):
        Mapper.__init__(self, width, height, resolution, topic)
        self.base_radius = base_radius
        self.barriers = {}
        self.barrierDetector = BarrierDetector();                
        
    def markBarrierAt(self, base_x, base_y, base_yaw):
        if self.barrierDetector.inCollision():
            barrierIdx = self.barrierDetector.sideBarrierIndex()
            barrier_x = base_x
            barrier_y = base_y + self.base_radius
            points = 5
            pi_div_6 = math.pi / 6.0            
            for i in range (0, points):
                
                if self.barrierDetector.collisionSide() != 'right':                    
                    alfa =  -pi_div_6 * barrierIdx - pi_div_6 * i / points
                else:
                    alfa =  pi_div_6 * barrierIdx + pi_div_6 * i / points
                
                after_rotation = self.rotate ( (base_x, base_y), (barrier_x, barrier_y), -yaw + alfa  )
                a_barrier_x = after_rotation[0]
                a_barrier_y = int(after_rotation[1])
                self.markDataFromDict (self.barriers, a_barrier_x, a_barrier_y)

class WeightMapper(Mapper):
    
    def __init__(self, width, height, resolution, topic, barrierMapper, visitMapper):
        Mapper.__init__(self, width, height, resolution, topic)
        self.barrierMapper = barrierMapper
        self.visitMapper = visitMapper
        self.barrier_weight = 4
        self.lWeight = 0
        self.rWeight = 0
        self.leftWeightPub = rospy.Publisher("left_weights", Int8, queue_size=10)
        self.rightWeightPub = rospy.Publisher("right_weights", Int8, queue_size=10)
        self.left = 0

    ##### Only tests
    def start(self, sx, sy, x, y):
        print "--", x, y
        return

    def end(self, x, y, obMapper):
        return
    ##### Only tests
        
    def fieldWeight(self, x, y):
        return self.visitMapper.valueOf(x, y) + self.barrier_weight * self.barrierMapper.valueOf(x, y)
        
    def updateLeftWeight(self, x, y):
        self.lWeight += self.fieldWeight(x, y)

    def updateRightWeight(self, x, y):
        self.rWeight += self.fieldWeight(x, y)

    def f (self, x, y):
        if self.left:
            self.updateData(x, y, self.lWeight)
        else:
            self.updateData(x, y, self.rWeight)

    def countAreaWeight(self, ym, xm, r1, direction, r1_axis, yaw):
        value = 0
        area_start = 0
        l = 0
        for i in r1:

            if i > 0:
                r2 = range(0, i + 1)
            else:
                r2 = range(i - 1, 0)
            
            for j in r2:
                # area_start + size, area_end + size + 1
            
                if r1_axis == 'x':
                    x = i + area_start + xm
                    y = direction * j + area_start + ym
                else:
                    x = direction * j + area_start + xm
                    y = i + area_start + ym
                          
                after_rotation = self.rotate ( (xm, ym), (x, y), -yaw  )
                a_barrier_x = int(after_rotation[0])
                a_barrier_y = int(after_rotation[1])

                w = self.fieldWeight (a_barrier_x, a_barrier_y)
                l += 1
                value += w
            
        v = min ( (value / l), 100)
        return v           

    def updateWeightMap(self, ym, xm, robotx, roboty, yaw):
        self.map.info.origin.position.x = robotx - (self.width * self.resolution)/2.0 
        self.map.info.origin.position.y = roboty - (self.height * self.resolution)/2.0
    
        size = self.width
        r1 = range(0, size + 1)

        x_y = self.countAreaWeight(xm, ym, r1, 1, 'x', yaw)
        y_x = self.countAreaWeight(xm, ym, r1, 1, 'y', yaw)
    
        y_m_x = self.countAreaWeight(xm, ym, r1, -1, 'y', yaw)
        r1 = range(-size - 1, 0)
        m_x_y = self.countAreaWeight(xm, ym, r1, -1, 'x', yaw)

        self.lWeight = int((x_y + y_x) / 2)
        self.rWeight = int((y_m_x + m_x_y) / 2)
        
        self.reset()
        
        after_rotation = self.rotate ( (0, 0), (self.width/4, self.width/4), -yaw  )
        self.left = 1
        self.traverseSquare (self.width/8+1, 1, int(after_rotation[0]), int(after_rotation[1]), self)
        self.left = 0
        after_rotation = self.rotate ( (0, 0), (-self.width/4, self.width/4), -yaw  )
        self.traverseSquare (self.width/8+1, 1, int(after_rotation[0]), int(after_rotation[1]), self)

        self.leftWeightPub.publish( self.lWeight )
        self.rightWeightPub.publish( self.rWeight )
        
    
visit_radius = 4
barrier_radius = 7

visitMapper = VisitMapper (MAP_WIDTH, MAP_HEIGHT, RESOLUTION, 'map', visit_radius )
barrierMapper = BarrierMapper(MAP_WIDTH, MAP_HEIGHT, RESOLUTION, 'barriers', barrier_radius)
visitObMapper = ObservationMapper(OB_MAP_WIDTH, OB_MAP_HEIGHT, OB_RESOLUTION, 'visit_squares', Sumator(visitMapper))
barrierObMapper = ObservationMapper(OB_MAP_WIDTH, OB_MAP_HEIGHT, OB_RESOLUTION, 'barrier_squares', Sumator(barrierMapper))

weightMapper = WeightMapper(W_MAP_WIDTH, W_MAP_HEIGHT, RESOLUTION, 'weights', visitMapper, barrierMapper)

rate = 10
cnt = 1
r = rospy.Rate(rate)

while not rospy.is_shutdown():
    
    current_time = rospy.Time.now()    
    fPublisher.publish(  visitMapper.f() )    

    if cnt % rate == 0:
        cnt = 0
        visitMapper.publishMap();
        barrierMapper.publishMap();        
        weightMapper.publishMap();
        #        visitObMapper.publishMap();
        #        barrierObMapper.publishMap();

    cnt += 1

    
    r.sleep()

    position, quaternion = g_positioner.getTransform()
    euler = tf.transformations.euler_from_quaternion(quaternion)
    yaw = euler[2]
    
    xx =  int (position[0] / RESOLUTION)
    yy =  int (position[1] / RESOLUTION)
    xm = MAP_WIDTH / 2 + xx
    ym = MAP_HEIGHT / 2 + yy
    
    visitMapper.markVisitAt (ym, xm)
    barrierMapper.markBarrierAt (ym, xm, yaw)
    weightMapper.updateWeightMap(ym, xm, position[0], position[1], yaw)
    #       visitObMapper.updateObMap(position[0], position[1], ym, xm)
    #       barrierObMapper.updateObMap(position[0], position[1], ym, xm)

