import multiple_runs

if __name__ == '__main__':

    strategy_combinations = []
    for i in range(0, 6):
        factor = i/2.0
        strategy_combinations.append ([1,   0,   0,   0,  0,  factor, "orto_d_{}".format(factor)])
                                        
    simulator = multiple_runs.Simulator(world_name = "hause2",
                                        show_gazebo = False,
                                        seconds = 15 * 60, # 10*60 # 10 minutes
                                        runs = 15, # * 20 minutes ->  300minutes -> 6h
                                        base_dir = "/media/wojtas/32986DF9986DBC4B/hause2_orto",
                                        x_ranges = [[-1.5, 1], [-3, -2.2], [-2.2, -1.2], [1.4, 2]], 
                                        y_ranges = [[-1, 0.5], [-3, -2],   [2, 3.2],     [2,   3]],
                                        #   Ort,  Par, Cir, Rnd, AreaWeightFactor, SensorDistanceFactor, name
                                        strategy_combinations = strategy_combinations)
    simulator.simulate ();
    simulator.plot ();
