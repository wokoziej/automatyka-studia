import multiple_runs
import os

if __name__ == '__main__':
    base_dir = "/media/wojtas/32986DF9986DBC4B/hause2_full2"
    strategy_combinations = []
    for p in range(1, 2):
        for o in range(1, 2):  
            for c in range (1, 2):
                for r in range (0, 2):
                    for w in range (0, 2):
                        weight_factor = w / 2.0
                        for f in range(0, 7):  
                            distance_factor = f / 2.0
                            #if r == 0 or distance_factor > 0.5: # We Crash after that so we want to omit 
                            if p != 0 or o != 0 or c !=0 or r != 0:
                                name = "o{}_p{}_c{}_r{}_wf_{}_df_{}".format(o, p, c, r, weight_factor, distance_factor)
                                directory = "{}/{}_range_i_0".format(base_dir, name)
                                print "directory {} exists?".format (directory), os.path.exists(directory)
                                if not os.path.exists(directory):
                                    strategy_combinations.append ([o, p, c, r, weight_factor, distance_factor,  name])

    simulator = multiple_runs.Simulator(world_name = "hause2",
                                         show_gazebo = False,
                                         seconds = 15*60, # 15*60
                                         runs = 5, # * 20 minutes ->  300minutes -> 6h
                                         base_dir = base_dir,
                                         x_ranges = [[-1.5, 1], [-3, -2.2], [-2.2, -1.2], [1.4, 2]], 
                                         y_ranges = [[-1, 0.5], [-3, -2],   [2, 3.2],     [2,   3]],
                                         #   Ort,  Par, Cir, Rnd, AreaWeightFactor, SensorDistanceFactor, name
                                         strategy_combinations = strategy_combinations)
    simulator.simulate ();
