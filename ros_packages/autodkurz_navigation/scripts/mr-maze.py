import multiple_runs

if __name__ == '__main__':
    simulator = multiple_runs.Simulator(world_name = "litle_maze",
                                        show_gazebo = True,
                                        seconds = 10 * 60, # 10*60 # 10 minutes
                                        runs = 20, # * 20 minutes ->  300minutes -> 6h
                                        base_dir = "/media/wojtas/32986DF9986DBC4B/hause_1",
                                        x_ranges = [[-1, -0.5], [0.5, 1]], 
                                        y_ranges = [[-1, -0.5], [0.5, 1]],
                                        #   Ort,  Par, Cir, Rnd, AreaWeightFactor, SensorDistanceFactor, name
                                        strategy_combinations = [
                                            [0,   0,   0,   1,  0,         1.0, "random"],
                                            [0,   0,   0,   1,  1,         1.0, "random_weight"],
                                            [1,   0,   0,   0,  0,         1.0, "orto"],
                                            [1,   0,   0,   0,  1,         1.0, "orto_weight"],
                                            [0,   1,   0,   0,  0,         1.0, "ara"],   
                                            [0,   1,   0,   0,  1,         1.0, "para_weight"],
                                            [1,   1,   0,   0,  0,         1.0, "orto_para"],
                                            [1,   1,   0,   0,  1,         1.0, "orto_para_weight"],
                                            [0,   0,   1,   0,  1,         1.0, "circ_weight"],    
                                            [1,   1,   1,   0,  False,     True, "orto_para_circ"],
                                            [1,   1,   1,   0,  True,      True, "orto_para_circ_weight"],
                                            [1,   1,   1,   1,  False,     True, "orto_para_circ_rand"],                                                                                           
                                            [1,   1,   1,   1,  True,      True, "orto_para_circ_rand_weight"]
                                        ])
    simulator.simulate ();
    simulator.plot ();
