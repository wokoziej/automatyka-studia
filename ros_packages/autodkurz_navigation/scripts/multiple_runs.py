#!/usr/bin/env python
import subprocess
import random
import time
import os
import visualize_runs
import yaml
import math

class Runner:
    def __init__(self, duration, directory, config, x_range, y_range, world_name, show_gazebo):
        self.duration = duration
        self.directory = os.path.abspath(directory)
        self.config_file_path = directory + "/config.yaml"  
        self.config = config
        self.x_range = x_range
        self.y_range = y_range
        self.world_name = world_name
        self.show_gazebo = show_gazebo
        
    def createConfigFile(self):
        with open(self.config_file_path, 'w') as outfile:
            yaml.dump(self.config, outfile, default_flow_style=False)
            outfile.close()
        
    def runCommandString (self, modelX, modelY, prefix, model_yaw):
        command = "timeout -k 20 {}  roslaunch autodkurz_navigation simulate.launch model_x:={} model_y:={} record_prefix:={} config_file:={} autodkurz_world_name:={} gui:={} model_yaw:={}"
        return command.format(self.duration, modelX, modelY, prefix, self.config_file_path, self.world_name, self.show_gazebo, model_yaw)

    def run(self, prefix):
        
        x = random.uniform(self.x_range[0], self.x_range[1])
        y = random.uniform(self.y_range[0], self.y_range[1])
        model_yaw  = random.uniform(0, 2 * math.pi)

        if not os.path.exists(self.directory):
            os.makedirs(self.directory)
            
        self.createConfigFile()
        outputDirAndPrefix = "{}/{}".format(self.directory, prefix)
        commandString = self.runCommandString(x, y, outputDirAndPrefix, model_yaw)
        return_code = subprocess.call(commandString, shell=True)

        

class Simulator:

    def __init__(self, world_name = "hause2",
                 show_gazebo = False,
                 seconds = 10*60, # 10*60 # 10 minutes
                 runs = 6, # * 20 minutes ->  300minutes -> 6h
                 #base_dir="/media/wojtas/32986DF9986DBC4B/autodkurz_runs_sens_9"
                 base_dir = "/media/wojtas/32986DF9986DBC4B/autodkurz_runs_sens_turn45_11",
                 # Diferent starts for machine 4 * 5h -> 20h
                 x_ranges = [[-1.5, 1], [-3, -2.2], [-2.2, -1.2], [1.4, 2]], 
                 y_ranges = [[-1, 0.5], [-3, -2],   [2, 3.2],     [2,   3]],
                 #   Ort,  Par, Cir, Rnd, AreaWeightFactor, SensorDistanceFactor, name
                 strategy_combinations = [
                     [0,   0,   0,   1,  0,         1.0, "random"],
                     [0,   0,   0,   1,  1,       1.0, "random_weight"],
                     [1,   0,   0,   0,  0,         1.0, "orto"],
                     [1,   0,   0,   0,  1,         1.0, "orto_weight"],
                     [0,   1,   0,   0,  0,         1.0, "ara"],   
                     [0,   1,   0,   0,  1,       1.0, "para_weight"],
                     [1,   1,   0,   0,  0,         1.0, "orto_para"],
                     [1,   1,   0,   0,  1,       1.0, "orto_para_weight"],
                     [0,   0,   1,   0,  1,         1.0, "circ_weight"],    
                     [1,   1,   1,   0,  False,     True, "orto_para_circ"],
                     [1,   1,   1,   0,  True,      True, "orto_para_circ_weight"],
                     [1,   1,   1,   1,  False,     True, "orto_para_circ_rand"],                                                                                           
                     [1,   1,   1,   1,  True,      True, "orto_para_circ_rand_weight"]
                 ]):
        self.world_name = world_name
        self.seconds = seconds
        self.runs = runs
        self.base_dir = base_dir
        self.x_ranges = x_ranges
        self.y_ranges = y_ranges
        self.strategy_combinations = strategy_combinations
        self.show_gazebo = show_gazebo
        
    def simulate (self):        
        # run duration [h] * runs[u] * strategy combinations [u] * starting points [u]
        # 20m              * 15      * 8                         * 4                 =  6days + plotting
        #    global strategy_combinations
        for strategy_combination_index in range(0, len(self.strategy_combinations)):
            strategy_combination = self.strategy_combinations [strategy_combination_index]
            ortogonal_strategy_runs = strategy_combination[0]
            parallel_strategy_runs  = strategy_combination[1]
            circular_strategy_runs  = strategy_combination[2]
            random_strategy_runs    = strategy_combination[3]
            area_weight_turn_factor  = strategy_combination[4]
            sensor_distance_turn_factor = strategy_combination[5]
            strategy_combination_name   = strategy_combination[6]
            
            for range_index in range(0, len(self.x_ranges)):
                config = {
                    'sensor_distance_turn_factor'      : sensor_distance_turn_factor,
                    'area_weight_turn_factor'     : area_weight_turn_factor,
                    'ortogonal_lines_strategy' : {'max_execution_runs': ortogonal_strategy_runs},
                    'parallel_lines_strategy'  : {'max_execution_runs': parallel_strategy_runs},
                    'circular_lines_strategy'  : {'max_execution_runs': circular_strategy_runs},
                    'random_lines_strategy'    : {'max_execution_runs': random_strategy_runs}            }
                name = "{}_range_i_{}".format(strategy_combination_name, range_index)
                directory = "{}/{}".format(self.base_dir, name)
                runner = Runner(duration = self.seconds,
                                directory = directory,
                                config = config,
                                x_range = self.x_ranges[range_index],
                                y_range = self.y_ranges[range_index],
                                world_name = self.world_name,
                                show_gazebo = self.show_gazebo)
                
                for i in range(0, self.runs):
                    print "================================================ : \t {} ".format(i)
                    runner.run(i)
                    time.sleep(30)
                

    # def plot(self, group_by = "strategy"):
    #     if group_by == "strategy":
    #         for strategy_combination_index in range(0, len(self.strategy_combinations)):
    #             strategy_combination = self.strategy_combinations [strategy_combination_index]
    #             strategy_combination_name = strategy_combination[6]
    #             mean_array = []
    #             stddev_array = []
    #             names = []                

    #             directory = "{}/{}_range_i*".format(self.base_dir, strategy_combination_name)                    

    #             visualize_runs.read_arrays(directory, "", mean_array, stddev_array, names)
    #             visualize_runs.plot_summary(self.base_dir, strategy_combination_name, mean_array, stddev_array, names)
    #     else:
    #         # I can plot on one graf all means and stddev for specified range start
    #         for range_index in range(0, len(x_ranges)):
    #             mean_array = []
    #             stddev_array = []
    #             names = []
    #             visualize_runs.read_arrays(directory, "*{}".format(range_index), mean_array, stddev_array, names)
    #             visualize_runs.plot_summary(self.base_dir, range_index, mean_array, stddev_array, names)

if __name__ == '__main__':
    simulator = Simulator()
    simulator.simulate ();
#    simulator.plot ();
    
