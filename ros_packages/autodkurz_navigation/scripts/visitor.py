#!/usr/bin/env python
import argparse
import random
import datetime
import rospy
from move_base_msgs.msg import MoveBaseAction, MoveBaseGoal
import actionlib
from actionlib_msgs.msg import *
import tf.transformations;
from geometry_msgs.msg import Twist
from sensor_msgs.msg import Range

import autodkurz_control
from autodkurz_control.msg import LimitSwitchState

from tf import TransformListener


## todo
## 1. Speed should depends on barrier distancce (see sonar_sensor)
## 2. If we hit barrier we should often we shoul increase turn time

parser = argparse.ArgumentParser()
parser.add_argument("--map_width",
                    help="Width of map",
                    type=float,
                    default=5.0)

parser.add_argument("--map_height",
                    help="Height of map",
                    type=float,
                    default=5.0)

parser.add_argument("--base_frame_id",
                    help="Id of robot base",
                    default="/base_footprint")

parser.add_argument("--map_frame_id",
                    help="Id of robot base",
                    default="/odom")

parser.add_argument("--collisions_topic",
                    help="Topic with colisions information",
                    default="/switches")

parser.add_argument("--time_to_wait_for_results",
                    help="How many seconds should we wait for getting results from move base",
                    type=int,
                    default=120)

parser.add_argument("--no_of_points_to_visit",
                    help="How many seconds should we wait for getting results from move base",
                    type=int,
                    default=10)

parser.add_argument("--strategy",
                    help="Exploration strategy: random - choose points coordination randomly, grid - map points as grid",
                    default="random",
                    choices=["random"])

class Visitor():
    
    def __init__(self, args):
        rospy.init_node('visitor', anonymous=False)
	rospy.on_shutdown(self.shutdown)
        
	self.base_frame_id = args.base_frame_id
        self.time_to_wait_for_results = args.time_to_wait_for_results
        self.map_frame_id = args.map_frame_id
        self.no_of_points_to_visit = args.no_of_points_to_visit
        self.strategy = args.strategy
        self.step = 0.3;
        self.collisions_topic = args.collisions_topic
        self.map_width = args.map_width;
        self.map_height = args.map_height;
        self.tf = TransformListener()        
        self.barrierDetected = False;
        self.rate = rospy.Rate(10)

        rospy.Subscriber('sonar', Range, self.rememberSonarRange)
        rospy.Subscriber('sonar_l', Range, self.rememberSonarRangeL)
        rospy.Subscriber('sonar_r', Range, self.rememberSonarRangeR)

        self.ranges = [1, 1, 1];
        self.ranges_l = [1, 1, 1];
        self.ranges_r = [1, 1, 1];

        
        self.collisionsSub = rospy.Subscriber(self.collisions_topic, LimitSwitchState, self.onBarrierDetected)
        self.cmdVelPublisher = rospy.Publisher('/auto/cmd_vel', Twist, queue_size=1)

        self.timeForMovingBack = 1;
        self.timeForTurn = 4;

        self.speed = 0.09
        self.turn = 0            
        self.turnSpeed = 0.7
        self.twist = Twist()
        self.twist.linear.x = self.speed;
        self.twist.linear.y = 0;
        self.twist.linear.z = 04
        self.twist.angular.x = 0;
        self.twist.angular.y = 0;
        self.twist.angular.z = 0;
        
        self.barrierDetectionTime = rospy.Time.now()
        self.prevBarrierDetectionTime = self.barrierDetectionTime
        print self.barrierDetectionTime, self.prevBarrierDetectionTime

#        exit(0)
        self.goToNextBarrier()

    def remeberRange(self, ranges, range):
        ranges[2] = ranges[1]
        ranges[1] = ranges[0]
        ranges[0] = range

    def rememberSonarRange(self, msg):
        self.remeberRange(self.ranges, msg.range)

    def rememberSonarRangeL(self, msg):
        self.remeberRange(self.ranges_l, msg.range)
        
    def rememberSonarRangeR(self, msg):
        self.remeberRange(self.ranges_r, msg.range)
        
    def onBarrierDetected(self, msg):
        if msg.open != True:
            rospy.loginfo("Barrier detected")
            self.barrierDetected = True;
            self.twist.linear.x = -self.speed;
            if msg.switch_id > 2:
                self.turn = -self.turnSpeed
            else:
                self.turn = self.turnSpeed
            self.barrierDetectionTime = rospy.Time.now();                
                
    # def defineNextGoal(self):

    #     try:
    #         t = self.tf.getLatestCommonTime(self.base_frame_id, self.map_frame_id)
    #         position, quaternion = self.tf.lookupTransform(self.map_frame_id, self.base_frame_id, t)                
    #         now = rospy.Time.now();                
    #         lastMoveTime = self.goal.target_pose.header.stamp;
    #         if abs(lastMoveTime - now) < rospy.Duration(1):
    #             stepMultiplier = self.step * 3
    #         else:
    #             stepMultiplier = self.step;
    #         self.goal.target_pose.header.stamp = now
                
    #         self.direction = 1
    #         x = position [0] + self.direction * stepMultiplier;
    #         y = position [1];
            
    #         self.goal.target_pose.pose.position.x = x
    #         self.goal.target_pose.pose.position.y = y
            
    #         angle = 1 #.random(
    #         axis = [0, 0, 1]
    #         q = tf.transformations.quaternion_about_axis(angle, axis);
            
    #         self.goal.target_pose.pose.orientation.x = q[0]
    #         self.goal.target_pose.pose.orientation.y = q[1]
    #         self.goal.target_pose.pose.orientation.z = q[2]
    #         self.goal.target_pose.pose.orientation.w = q[3]
            
    #         return 1
    #     except Exception, e:
    #         rospy.loginfo('E: '+ str(e))            
    #     return 0

    def speedAccordingToRanges(self):
        return (sum(self.ranges)   / float(len(self.ranges  )) +
                sum(self.ranges_r) / float(len(self.ranges_r)) +
                sum(self.ranges_l) / float(len(self.ranges_l)) ) / 3.0
          
        
    def goToNextBarrier(self):
        
        timeFromBarrierDetection = rospy.Time.now() - self.barrierDetectionTime

        print self.barrierDetectionTime, self.prevBarrierDetectionTime

                
        if timeFromBarrierDetection > rospy.Duration(self.timeForMovingBack + self.timeForTurn):
            
            if (self.barrierDetectionTime - self.prevBarrierDetectionTime) < rospy.Duration(5):
                self.timeForTurn = 4;
            else:
                if (self.barrierDetectionTime - self.prevBarrierDetectionTime) < rospy.Duration(9):
                    self.timeForTurn = 5;
                else:
                    self.timeForTurn = 6;

            self.prevBarrierDetectionTime = self.barrierDetectionTime
            mult = self.speedAccordingToRanges() + 0.09
            self.twist.linear.x = self.speed * mult;
            self.twist.angular.z = 0;
            rospy.loginfo("Going forward " + str(self.twist.linear.x))

        else:
            if timeFromBarrierDetection < rospy.Duration(self.timeForMovingBack):
                self.twist.angular.z = 0;
                rospy.loginfo("Moving back");
            else:
                rospy.loginfo("Changing direction " + str (self.timeForTurn))
                self.twist.linear.x = 0;
                self.twist.angular.z = self.turn;

        self.cmdVelPublisher.publish(self.twist)
        self.rate.sleep()

    def shutdown(self):
        rospy.loginfo("Stop")

    
    def visit(self):
        while True:
            self.goToNextBarrier()
        
if __name__ == '__main__':
    try:
        args = parser.parse_args()
        visitor = Visitor (args)
        visitor.visit()
        
    except rospy.ROSInterruptException:
        rospy.loginfo("Exception thrown")

