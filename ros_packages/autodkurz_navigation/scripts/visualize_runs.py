#!/usr/bin/env python
# -*- coding: utf-8 -*-
import rosbag
import matplotlib.pyplot as plt
import os
import rospy
import numpy 
import sys
import glob
import re
import yaml
import sys

plt.rc('font', **{'sans-serif' : 'Arial',
                         'family' : 'serif'})

reload(sys)

sys.setdefaultencoding('utf8')
class BagCollector:
    def __init__ (self, directory):
        self.array_of_arrays = []
        self.no_of_messages = 0
        self.directory = directory

    def collect_all(self):
        for dirname, dirnames, filenames in os.walk(self.directory):
            for d in dirnames:
                self.collect(d)
        
    def collect(self, subdir, startSec = None, stopSec = None, save = True):
        files = os.popen('ls -1t {}/{}/*.bag'.format(self.directory, subdir)).read()
        files_array = files.split('\n')
        self.array_of_arrays = []
        
        for file_name in reversed(filter(None, files_array)):
            print os.popen('date').read(), file_name
            values = []
            bag = rosbag.Bag(file_name)
            
            s = bag.get_start_time() if startSec == None else bag.get_start_time() + startSec
            e = bag.get_end_time() if stopSec == None else bag.get_start_time() + stopSec
            start = rospy.Time().from_seconds( s)
            end = rospy.Time().from_seconds( e )
            for topic, msg, t in bag.read_messages(topics = ['/fun'],  start_time = start, end_time = end ):
                values.append(msg.data)
            bag.close()
            self.array_of_arrays.append(values)
            self._normalizeSizes()
        if save:
            return self._save('{}/{}/'.format(self.directory, subdir))

    def _normalizeSizes(self):
        # Make the same size of subarrays 
        min_length = sys.maxint
        for sub in self.array_of_arrays:
            l = len (sub)
            if l < min_length:
                min_length = l
        i = 0
        for sub in self.array_of_arrays:
            self.array_of_arrays[i] = sub[:min_length]
            i += 1

        self.no_of_messages = min_length
        
    def _save(self, directory):    
        mean = numpy.mean(self.array_of_arrays, axis=0)
        stddev = numpy.std(self.array_of_arrays, axis=0)
        x = numpy.arange(self.no_of_messages)
    
        if len(x):
            print "Saving txt in {}".format(directory)
            plt.gcf().clear()
            plt.errorbar(x, mean, stddev, linestyle='None', marker='^')
            plt.savefig ("{}/{}".format(directory, "mean_and_stddev.eps"))
            numpy.savetxt("{}/{}".format(directory, "mean.txt"), mean)
            numpy.savetxt("{}/{}".format(directory, "stddev.txt"), stddev)
            
        return mean, stddev

    
class TxtPlotter:
    def __init__(self, directory):
        self.directory = directory

    def read_arrays(self, subdir_mask):
        mean_array = []
        stddev_array = []
        names = []
        mask = "{}/{}/*.txt".format(self.directory, subdir_mask)
        print mask
        files = glob.glob(mask)
        files.sort ()
        for f in files:
#            print f
            if "mean.txt" in f:
                c = re.search(".*/(.+?)/mean.txt", f)
                name = c.group(1)
                mean = numpy.loadtxt(f)
                if len(mean) > 0:
                    mean_array.append(mean)
                    names.append(name)
            else:
                if "stddev.txt" in f:
                    stddev = numpy.loadtxt(f)
                    if len(stddev) > 0:
                        stddev_array.append(stddev)
        return names, mean_array, stddev_array
            
    def plot(self, subdir_masks, safe_to_file_name, points_to_skip = 1, xaxis_scale = 10):

        names = []
        means = []
        stddevs = [] 
        for m in subdir_masks:
            dir_name = m
            line_name = ""
            if isinstance(m, dict):
                dir_name = m.keys()[0]
                line_name = m.values()
            n, m, s = self.read_arrays(dir_name)                            
            names += n if len(line_name) == 0 else line_name
            means +=  m
            stddevs += s
        print "<<<", names    
        plt.gcf().clear()
        i = 0
        for mean, name in zip (means, names):
            if len (mean):
                x = numpy.arange( len(means[i]) / xaxis_scale )
                m = mean[0:len(mean):points_to_skip]
                stddev = stddevs [i]
                s = stddev[0:len(stddev):points_to_skip]                
                x = x [0:len(x):points_to_skip / xaxis_scale]                
                plt.errorbar(x, m, s, linestyle='None', marker='^', label=name)
                plt.xlabel('czas [sec]')
                plt.ylabel('f(k)')
                i += 1
                print "Plotted {}".format(name)
            else:
                print 'empty!:', name
        plt.legend(bbox_to_anchor=(0., 1.02, 1., .102), loc=2,
                   ncol=2, mode="expand", borderaxespad=0.)
        if len(safe_to_file_name) > 0:
            plt.savefig ("{}/{}.jpg".format(self.directory, safe_to_file_name))

            
    def read_results_from_dir(self, directory):
        config = yaml.safe_load(open("{}/{}/config.yaml".format(self.directory, directory)))
        _pos = directory.rfind("_")
        starting_range = directory [_pos + 1:]        
        visited_factor = config.get('area_weight_turn_factor', 0)
        distance_factor = config.get('sensor_distance_turn_factor', 0)
        para = config['parallel_lines_strategy'].get('max_execution_runs', 1)
        orto = config['ortogonal_lines_strategy'].get('max_execution_runs', 1)
        circ = config['circular_lines_strategy'].get('max_execution_runs', 1) 
        rand = config['random_lines_strategy'].get('max_execution_runs', 1) 

        mean = numpy.loadtxt("{}/{}/mean.txt".format(self.directory, directory))
        avg = mean[-1]
        stddev = numpy.loadtxt("{}/{}/stddev.txt".format(self.directory, directory))
        worst = - stddev[-1] + avg
        best =    stddev[-1] + avg
        return int(starting_range), para, orto,  circ, rand, visited_factor, distance_factor, worst, avg, best 
                
        
        # return {"range": int(starting_range),
        #         "orto" : orto,
        #         "para" : para,
        #         "circ" : circ,
        #         "rand" : rand,
        #         "visited_factor" : visited_factor,
        #         "distance_factor" : distance_factor,
        #         "worst" : worst,
        #         "avg" : avg,
        #         "best" : best }
                

    def _results_to_txt(self, sorted_by_avg, file_name):
        file = open("{}/{}".format(self.directory, file_name), 'w')
        file.write("{:^30}".format("dir_name"))
        file.write("{:>4}".format("RANG"))
        file.write("{:>4}".format("par"))
        file.write("{:>4}".format("ort"))
        file.write("{:>4}".format("cir"))
        file.write("{:>4}".format("ran"))

        file.write("{:>6}".format("vis_f"))
        file.write("{:>6}".format("dis_f"))

        file.write("{:<10}".format("worst"))
        file.write("{:<10}".format("avg"))
        file.write("{:<10}".format("best"))
        file.write("\n")
        
        for item in sorted_by_avg:
            # name
            file.write("{:>30}".format(item[0]))
            # range
            for i in item[1:6]:
                file.write("{: 4d}".format(i))

            for i in item[6:8]:
                file.write("{: 6.1f}".format(i))

            for i in item[8:]:
                file.write("{: 10.2f}".format(i))
            file.write("\n")            
        #file.writelines(["%s\n" % item for item in results])
        file.close()

    def _results_to_tex(self, sorted_by_avg, file_name):
        file = open("{}/{}".format(self.directory, file_name), 'w')
        file.write("""
\\begin{table}[h]
\centering
\caption[Wyniki testów symulacji różnych strategii w obszarach testowych]{Wyniki testów symulacji różnych strategii w obszarach testowych. Dane posortowane wg kolumny ``Średni''}
\label{tab:WynikiSymulacji}
\\begin{tabular}{@{}l|l|l|l|l|r|r|r|r|r@{}}
\\toprule
\multicolumn{7}{l|}{Parametry}
& \multicolumn{3}{l}{Wyniki} \\\\
\midrule
\multirow{2}{*}{
  \\begin{sideways}
    %  \makecell{Numer\\\\ obszaru}
    Numer obszaru    
  \end{sideways}}
& \multicolumn{4}{l|}{ Strategia}                                                 & \multicolumn{2}{l|}{\makecell{Współczynnik\\\\ uwzględniania}} & Najgorszy     & Średni  & Najlepszy   \\\\
& \\begin{sideways}\makecell{ścieżki\\\\równoległe}\end{sideways} & \\begin{sideways}\makecell{ścieżki\\\\ prostopadłe}\end{sideways} & \\begin{sideways}wir\end{sideways} & \\begin{sideways}ścieżki losowe\end{sideways} &
\\begin{sideways}\makecell{wcześniejszych\\\\odwiedzin}\end{sideways}
          &  \\begin{sideways}odległości\end{sideways}                                      &               &         &    \\\\
\midrule
""")
        for item in sorted_by_avg:
            # name
            #            file.write("{:>30}".format(item[0]))
            # range
            file.write("{} &".format(item[1]+1))

            for i in item[2:6]:
                str  = ""
                if i == 1:
                    str = "$\\bullet$"
                file.write("{} &".format(str))
                
            for i in item[6:8]:
                file.write("{: 6.1f} &".format(i))

            for i in item[8:10]:
                file.write("{: 10.1f} &".format(i))
            file.write("{: 10.1f} \\\\ ".format(i))                
            file.write("\n")

        file.write("""  \\bottomrule
        \end{tabular}
        \end{table}
        """)

        file.close()

        
    def table_summary (self, file_name):
        # Traverse all directories and write results to table
        # RANGE | ORTO_USED | PARA_USED | CIRC_USED | RANDOM_USED | VISITED_FACT | DISTANCE_FACT | WORST | AVG | BEST
        results = []
    
        for dirname, dirnames, filenames in os.walk(self.directory):
            for d in dirnames:
                results.append( (d,) + self.read_results_from_dir(d))

        AVG_IDX = 9
        sorted_by_avg = sorted(results, key=lambda tup: tup[AVG_IDX])
        self._results_to_txt(sorted_by_avg, "{}.txt".format(file_name))
        self._results_to_tex(sorted_by_avg, "{}.tex".format(file_name))
        
            
if __name__ == '__main__':
    #directory = "/media/wojtas/32986DF9986DBC4B/hause2_orto"
    #directory = "/media/wojtas/32986DF9986DBC4B/autodkurz_runs_sens_turn45_9"

    directory = "/media/wojtas/32986DF9986DBC4B/hause2_full2"
    bagCollector = BagCollector(directory)
    #bagCollector.collect_all()       

    e = 0
    n = 0
    for p in range(0, 2):
        for o in range(0, 2):  
            for c in range (0, 2):
                for r in range (0, 2):
                    for w in range (0, 2):
                        weight_factor = w / 2.0
                        for f in range(0, 7):  
                            distance_factor = f / 2.0                            
                            if (p != 0 or o != 0 or c !=0 or r != 0):
                                for i in range(0, 4):                    
                                    name = "o{}_p{}_c{}_r{}_wf_{}_df_{}".format(o, p, c, r, weight_factor, distance_factor)
                                    sub = "{}_range_i_{}".format(name, i)
                                    #print directory, sub
                                    path = "{}/{}".format(directory, sub)
                                    if not os.path.isfile("{}/mean.txt".format(path)):
                                        #print path
                                        #try:
                                        #    os.rename(path, path + "_bad")
                                        #except OSError:
                                        #    print "Exc"

                                        try:
                                            bagCollector.collect(sub)
                                        except:
                                            print(path)
                                    else:
                                        print "Done"
                                        #          os.path.join(basedir, surname + ', ' + firstname))
                                    
    print("e {}, n {}".format(e, n))
    exit(0)
    
    # txtPlotter = TxtPlotter(directory)
    # for i in range(0, 4):
    #     run_mask = []
    #     for j in range(0,6):
    #         dist_factor = j / 2.0
    #         run_mask.append({"orto_d_{}_range_i_{}".format(dist_factor, i) : "dist_fact={}".format(dist_factor)})
    #     print run_mask 
    #     txtPlotter.plot(run_mask, "dist_fact_in_range_{}".format(i), 100)


    txtPlotter = TxtPlotter(directory)
    for i in range(0, 4):                    
        run_mask = []
        p = 0
        o = 0
        c = 0
        for r in range (0, 2):
            for w in range (0, 2):
                weight_factor = w / 2.0
                for f in range(0, 7):  
                    distance_factor = f / 2.0
                    if p != 0 or o != 0 or c !=0 or r != 0:
                        name = "o{}_p{}_c{}_r{}_wf_{}_df_{}".format(o, p, c, r, weight_factor, distance_factor)
                        print name
                        run_mask.append({"{}_range_i_{}".format(name, i) : "name={}".format(name)})
        txtPlotter.plot(run_mask, "{}_r_{}".format(name, i), 100)

    txtPlotter.table_summary ("results")  
        
    # for i in range(0, 5):
    #     # run_mask = ["random_weight_range_i_{}".format(i),
    #     #             "circ_weight_range_i_{}".format(i),
    #     #             "orto_weight_range_i_{}".format(i),
    #     #             "para_weight_range_i_{}".format(i),]
    #     # txtPlotter.plot(run_mask, "rand_vs_circ_vs_orto_vs_para_range_{}".format(i), 100)
    #     run_mask = [{"random_range_i_{}".format(i) : "losowe"},
    #                 {"circ_weight_range_i_{}".format(i) : "wir" },
    #                 {"orto_range_i_{}".format(i) : "prostopadłe"},
    #                 {"para_range_i_{}".format(i) : "równoległe"}]

    #     txtPlotter.plot(run_mask, "rand_vs_circ_vs_orto_vs_para_range_{}".format(i), 100)
        
#        subdir = "orto_d_{}_range_i_3".format(i/2.0)

#        bagCollector.collect(subdir)        
#        run_mask.append({subdir : "{}".format(i)})
                
    
#    txtPlotter.plot(run_mask, "rand_vs_circ_vs_orto_vs_para_range_{}".format(i), 100)
#    txtPlotter.table_summary ("results")
    


        
    #dir = '/media/wojtas/9e95e7be-2426-4913-ac7f-95781b3fbbab/bagfiles'
    #dir = "/media/wojtas/9e95e7be-2426-4913-ac7f-95781b3fbbab/bagfiles/no_area_weights"
#    directory = "/media/wojtas/32986DF9986DBC4B/autodkurz_runs_sens_8"
#    directory = "/media/wojtas/32986DF9986DBC4B/autodkurz_runs_sens_turn45_9"

    
    # mean_array = []
    # stddev_array = []
    # names = []
    # range_index = "3"

    # prepare_means_and_stddev_files(directory)
    # for range_index in range(0, 4):
    #     if True or range_index != 2:
    #         mean_array = []
    #         stddev_array = []
    #         names = []                
        
    #         read_arrays(directory, "*{}".format(range_index), mean_array, stddev_array, names)
    #         plot_summary(directory, range_index, mean_array, stddev_array, names)
    
    
